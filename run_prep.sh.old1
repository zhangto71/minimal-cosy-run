#!/bin/bash
# chmod +x run_prep.sh ; ./run_prep.sh office
# The chmod command may be needed before running any script.
#
# Run this script to prepare symbolic links to files and directories needed 
#  to run the .fox (e.g. aris.fox) cosy script. The script may need to be 
#  customized depending on the system that one is running from and what 
#  links and files are needed for the particular .fox application.
# Then use runcosy91.sh to run your .fox application by using
# $ runcosy91.sh aris.fox
# The script runcosy91loc.sh is similar, but has extra code that allows moving 
#  generated file to a subdirectory called out-? (?=root name of .fox file). 
#  Use with caution as it can delete/mv certain files that one 
#  may not have intended. Modify the .sh codes as needed.
# Note that the paths to certain files may need to be changed in the .fox script.
#  The most common one is the one used by ATMSSD for finding the spline files
#  used by the ATIMA subroutines.

# The following defines the directory which has the executable cosy on your 
#  own system. Be sure to update your runcosy...sh script to use the same path.
if_not_exist_ln() { if ! [ -a $2 ]; then ln -s $1 $2 ; echo "ln -s $1"; fi }
if_not_exist_cp() { if ! [ -f $2 ]; then cp $1/$2 . ; echo "cp $1/$2 ."; fi }
if_not_exist_cp_and_chmod() { if ! [ -f $2 ]; then cp $1/$2 . ; chmod +x $1 ; echo "cp $1/$2 ."; fi }

system=$1 ;
echo "  Setting up files and links for"
if [ "$system" == "ftc" ]; then
  echo "    $system: FTC network";
  cosypath="/usr/bin/" ;
  supp_dir="/files/shared/ap/aris-dev"
  optics_dir="/departments/AcceleratorPhysics/ARIS/Optics/aris-optics"
  if_not_exist_ln $cosypath cosy  #directory cosy executable
  if_not_exist_ln /usr/lib/cosy/COSY91.bin lnCOSY91.bin
elif [ "$system" == "office" ]; then
  echo "    $system: Office network (e.g. fisthtank)";
  cosypath="/projects/aris/cosy/cosy_stash" ;
  #supp_dir="/departments/AcceleratorPhysics/ARIS/Optics/aris-dev"
  supp_dir="/departments/AcceleratorPhysics/ARIS/Optics/aris-dev-portillo"
  optics_dir="/departments/AcceleratorPhysics/ARIS/Optics/aris-optics"
  if_not_exist_ln $cosypath cosy
  cosypath="cosy"
  if_not_exist_ln /projects/aris/cosy/cosy_stash/COSY91.bin lnCOSY91.bin
else
  echo "$system: invalid entry";
  system="office"
  exit 1
fi


if_not_exist_ln $supp_dir aris-dev  #directory with support files
supp_dir="aris-dev"

if_not_exist_ln $optics_dir aris-optics  #directory with support files
optics_dir="aris-optics"

#copy files and setup links
#--- the basics
if_not_exist_ln $supp_dir/param_files param_files
if_not_exist_ln $supp_dir/SYSCA.DAT SYSCA.DAT
if_not_exist_cp_and_chmod $supp_dir process_SLOG.sh
if_not_exist_cp_and_chmod $supp_dir runcosy91.sh
if_not_exist_cp_and_chmod $supp_dir runcosy91.py
if_not_exist_cp_and_chmod $supp_dir tools.py
if_not_exist_cp_and_chmod $supp_dir clean.sh
#cp $supp_dir/run.sh . ; chmod +x run.sh  (not sure this works as intended)
if_not_exist_ln $supp_dir/rootapps rootapps
if_not_exist_ln $supp_dir/scale_and_refit_process_PS scale_and_refit_process_PS
if_not_exist_ln $supp_dir/rayplot_cosy8anl rayplot_cosy8anl
if_not_exist_ln $supp_dir/scripts scripts
# pre- and post-processing
#if_not_exist_cp $supp_dir mpole_data_PROCESS.C (see rootapps)

# TASK=1.10 : Run cosy and FIT at single or multiple fields
if_not_exist_cp_and_chmod $supp_dir task_FIT.py
if_not_exist_cp_and_chmod $supp_dir task_gen.py

#temporary since OS command in COSY is not functioning like before
#mkdir wm_cosymaps lise_cond lise_ext
exit

if_not_exist_cp_and_chmod $supp_dir task_1.10_PS_FIT.py 
if_not_exist_cp_and_chmod $supp_dir task_1.10_PS_REFIT.py

ls -l aris-dev

# options to fit PS over Brho range (Optional as these as python methods exists above)
#if_not_exist_cp_and_chmod2 $supp_dir/fit_rng_task_1.10_PS_SCL_FIT_sed.sh
#if_not_exist_cp_and_chmod2 $supp_dir/fit_rng_task_1.10_PS_NOSCL_FIT_sed.sh
#if_not_exist_cp_and_chmod2 $supp_dir/fit_rng_task_1.10_PS_SCL_NOFIT_sed.sh
#if_not_exist_cp_and_chmod2 $supp_dir/fit_rng_task_1.10_PS_multi_run_SCL_then_FIT.sh
#if_not_exist_cp_and_chmod2 $supp_dir/fit_rng_task_1.10_PS_multi_run_FIT.sh
