#!/usr/bin/python3
# clear; chmod +x read_data_n_set_pv.py ; ./read_data_n_set_pv.py
import os, sys
from epics import PV
#p = PV('FS_F1S1:PSQ_D1013:I_CSET')
#p.put(50.0)
#exit()

strc=""
read_data=True; cy=True; c1=True; c2=True; c3=True
N2 = 0; pL2 = []; name_sec = []; name_dev = []; val = []
if (read_data):
  # read from .csv file
  fp = open("PSv15[124Xe_4.3904Tm_DB0_DB1-fit1]_AMPS.csv", "r")
  str1 = '-'; str2 = '-'
  while( str1 != ''):
    str1 = fp.readline(); str1 = str1.rstrip('\n')
    flist1 = str1.split(',')
    if(len(flist1[0])>0):  #needed to avoid list with lest elements from other lines
       cy=False
       if (flist1[4]=='y'): cy=True
       c1=False
       if ("Q_D" in flist1[0]): c1=True
       c2=False
       if ('2' in flist1[2]): c2=True
    #if( "Q_D" in flist1[0] ):  #all lines with 4 elements
    #if(flist1[4]=="y"): #fails since some lines don't have 5 elements
    if( c2 and cy ):
      N2 = N2+1
      str2 = flist1[0]
      flist2 = str2.split(':')
      name_sec.append(flist2[0])
      name_dev.append(flist2[1])
      val.append(flist1[3])
  #print(pL2)
  #print(val)
  fp.close()

#--- generate PV commands based on values read
set_type="I_CSET"
#
if (set_type=="I_CSET"):
  #val = [ 150.0, 130.0 ]
  for i in range(0,N2):
    strc =  name_sec[i] + ":PS" + name_dev[i] + ":" + set_type    
    print( str(i) + ":  " + strc + ", " + set_type + "="+ str(val[i]) )
    print("p = PV('" + strc + "')")
    print("p.put(" + str(val[i]) + ")")
    p = PV(str(strc))
    p.put(val[i])
    print("")
exit()