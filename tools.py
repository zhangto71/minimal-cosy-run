import os, shutil

def sed_multi(arr_old, arr_new, f_old, f_new=None):
  opr = ''
  if f_new==f_old:
    print(" sed_multi: f_new==f_old not allowed. No action taken.")
    return
  for i in range(0,len(arr_old)):
    print( str(i) + "   " + arr_old[i] + "   " + arr_new[i] )
    opr += "-e 's/" + arr_old[i] + "/" + arr_new[i] + "/g' "
  if f_new:
    os.system("sed " + opr + f_old + " > " + f_new)
  else:
    print(" sed_multi: Must provide new file (f_new) as result file")
    print("            Replacements aborted.")
  return
                                
def gen_list_sheet_1( sheet, keycol, keystr, listi, listf ):
  # Generate listi, listf of string substitutions based on __
  #   filexls - name of spreadsheet file (including path)
  #   wb      - spreasheet workbook reference
  #   sheet   - spreadsheet sheet reference
  #   keycol  - column with key string reference 
  #   keystr  - reference string to look up under kecol
  #   listi   - list of initial (existing) string
  #   listf   - list of replacement (desired) string

  Nrow = sheet.nrows                          
  Ncol = sheet.ncols
  keyFound = False
  if keyFound==False:
  #if sheetname==sheet.cell_value(1,1):  #optional; to avoid wrong sheet
  
    # search for row in xls with variable to fit
    row1 = -1  #initialize
    for i in range(keycol,Nrow):
      if sheet.cell_value(i,keycol)==keystr:
        row1 = i;
        print("     Select fit at row1= "+ str(row1) )
        keyFound = True
        break
    colV = 3
    COSY_VAR = sheet.cell_value(row1,colV)
    ELEM = sheet.cell_value(row1,2)
    print("     COSY_VAR: "+COSY_VAR)   #example: PM03Q4
    print("     ELEM: "+ELEM)           #example: Q_D1170
    MISALIGN = "M_MISALIGN 1  DX_M1 DY_M1 0  THX_M1 THY_M1 0 ;"

    #list headers (can be ignored, but good for list reference)
    listi.append('list__i')
    listf.append('list__f')
    
    # ---- replace field value
    coli=4; colf=15
    str1 = "{PARAMS-OVERRIDE}"
    listi.append(str1)
    listf.append( str1+COSY_VAR+" := "+str(sheet.cell_value(row1,colf))+" ; " )
    
    # ---- output field result (after conversion to Tesla)
    listi.append('{STRL_ADD}')
    # note use of octal \o047 to include single quotes
    listf.append("\&\o047,"+COSY_VAR+",\o047\&SF("+COSY_VAR+",FMTE)")

    # ---- .dat file name prefix to get field (converted from Amps)
    str1 = "FIELD_FILE_"
    listi.append(str1)
    listf.append(str(row1+1)+'-'+COSY_VAR+'__')

    # ---- set up FIT output with xls row reference
    listi.append(' FIT1_ROW ')
    listf.append(str(row1+1))
    
    # ---- replace for FIT objective (condition)
    col3i=23; col3f=24
    listi.append(sheet.cell_value(row1,col3i))
    listf.append(sheet.cell_value(row1,col3i)+sheet.cell_value(row1,col3f))
    
    # ---- insert MISALIGN
    coli=4; colf=14
    str1 = "{"+sheet.cell_value(row1,coli)+"}"
    listi.append(str1)
    listf.append( str1+MISALIGN )
    
  else:
    print("gen_list_1: Wrong sheet. Aborting.\n"); exit()
  return keyFound


def cp_file(fname1, dir2):
  #copy fname1 to dir2 if it does not exists
  if not os.path.exists(dir2):
    print("ERROR Directory does not exists: "+ dir2)
  fname2 = dir2+"/"+fname1 ;
  if not os.path.isfile(fname2):
    print("    Copy file: "+fname2)
    shutil.copyfile(fname1, fname2)
  return

def list_to_file(list1, flist):
  str1=''
  for i in range(len(list1)):
    str1 += str(list1[i]) + '\n'
  fp = open(flist, 'w'); fp.write(str1); fp.close()
  return

def reset_dir(dir1):
  if os.path.exists(dir1):
    shutil.rmtree(dir1)
  os.mkdir(dir1)
  return

def run_cosy_script(rc,cfox1,cfox2,initf,chim,flist):
  # generate list of old-new pairs for sed replacement in .fox file
  listi=[]; listf=[]
  #list headers (can be ignored, but good for list reference)
  listi.append('DUMMY_NAME')
  listf.append(initf)
  str1 = "CHIM_{:.4f}".format(chim)
  cfox2 = str1+"_"+cfox2
  listi.append('PS_NEW')
  listf.append(str1)
  sed_multi( listi, listf, cfox1, cfox2 )
  oscom = "chmod +x "+rc+" ; ./"+rc+" "+cfox2+" < "+flist
  os.system(oscom)
  # wait for cosy process to finish
  #-- while test=='RUN':
  #--   fp = open('CONTROL', 'r'); test = fp.readline(); fp.close()
  #--   time.sleep(1)
  #--   print('running')
  return

def mkdir_and_move_files( newdir, chim ):
  newsub = "out_"+"{:.4f}".format(chim); reset_dir(newsub)
  os.system("mv c*.png c*.gif GLOBAL_MAPS.TXT OBJ.TXT pic*.ps "+newsub)
  shutil.move(newsub,newdir)
  return
