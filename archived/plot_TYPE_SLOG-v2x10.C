{
//
//  2016-09, Portillo -- Plot data in SLOG.TXT of beam properties versus SPOS
//   This version needs to be run from the CINT environment
//
// Need to do for improvements:
//  Add ME(3,6) to COSY output and ANG related to orientation of dispersive plane
//  Add PX_DISP, PY_DISP = xm & ym envelopes that include dispersion
//  Plost both PX and PX_DISP for beam envelopes (also PY and PY_DISP
    gROOT->Reset();

  bool do_cPlotBmEnv=true;  //beam envelope plot
  bool do_cPlotAberr=false; //selected aberrations along s
  bool do_cPlotOrb=false;   //position of reference particle along s
  bool do_cRayPlots=false;   //COSY PP rays plots from .ps (ROOT5 only)
  int cycles=1000; int delay=5;
  
  /*
  for(int ii=0; ii<cycles; ii++){
  //ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  //ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    printf(" iter %d [delay %d sec]  [Ctrl+C to stop]\n", ii, delay);
    gSystem->Sleep(delay*1000);
  //ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  //ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  } //end
  */

  
  Float_t apt_fact = 1. ;   // factor to apply to apertures for certain plots
                            // (e.g. for aberrations)
  Float_t fact_ME_16=0.05;   // factor on dispersion term ME(1,6) for plot
  int ir;
  int SPOS=(1 -1);
  ir=8;
  int APTX=(ir+0 -1);
  int APTY=(ir+1 -1);
  int APTSTAT=(ir+2 -1);
  ir=24;
  int PX=(ir+0 -1);
  int PY=(ir+1 -1);
  int PX_DXD=(ir+2 -1);
  int PY_DYD=(ir+3 -1);
  ir=28;
  int X0=(ir+0 -1);
  int A0=(ir+1 -1);
  int Y0=(ir+2 -1);
  int B0=(ir+3 -1);
  int L0=(ir+4 -1);
  int D0=(ir+5 -1);
  int ME1_6=(38 -1);
  int ME3_6=(44 -1);
  //Define plot limits
  Float_t rMax=0.250, rMin=-rMax;

    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(kTRUE);  // plot WITHOUT stat window
    gStyle->SetPalette(1);



  Int_t i, j; //counter
  Int_t N; //number of lines read
  Int_t NC; //number of columns read
  const Int_t Nmax = 5000; //max lines
  const Int_t Cmax = 200; //max columns
  Int_t N0; Float_t x[Nmax], y[Nmax];
  Int_t N1; Float_t x_1[Nmax], y_1[Nmax], y_1f[Nmax];
  Int_t N2; Float_t x_2[Nmax], y_2[Nmax], y_2f[Nmax];
  Float_t data[Nmax][Cmax];
  TString Cnames[Cmax]; char* cp;
  char tmp[50];

  //Read until some endstring is encountered
  FILE* fp; char buffer[200];
  char fname[100]="SLOG.TXT";
  char endstr[20]="endline";
  int TheEnd=0 ;
  fp = fopen(fname, "r");
  if (fp==NULL) { printf("File not found: %s\n",fname); exit(0); }
  //Read column names
  while (!feof(fp) && !TheEnd){
  	fgets(buffer,sizeof(buffer),fp);
  	if( strncmp(buffer,endstr,7)==0 ) {
  	  TheEnd=1 ;
  	} else {
  	  buffer[strlen(buffer)-1]='\0'; //Remove \n from end of string
  	  Cnames[NC] = buffer;
      //cout << Cnames[NC] << endl ;
  	}
  	NC++;
  }
  //Read data lines
  N=0;
  while (!feof(fp)){
   TheEnd=0;
   N++; i=0;
   while (!TheEnd){
    fgets(buffer,sizeof(buffer),fp);
    sscanf(buffer,"%f\n",&data[N][i]);
    i++;
    if( strncmp(buffer,endstr,7)==0 ) TheEnd=1 ;
   }
  }
  fclose(fp);
  printf("  Lines read N= %i \n", N);
  printf("  Columns per line NC= %i \n", NC);

// Process apertures
  //APTX
  N1=0; x_1[N1]=0; y_1[N1]=0;
  for( i=0; i<N; i++){
  	if( data[i][APTSTAT]==1 ) { //ENTRANCE aperture
  	 N1++;  x_1[N1]=data[i][SPOS];  y_1[N1]=0.;
  	 N1++;  x_1[N1]=data[i][SPOS];  y_1[N1]=data[i][APTX];
  	}
  	if( data[i][APTSTAT]==2 ) { //EXIT aperture
  	 N1++;  x_1[N1]=data[i][SPOS];  y_1[N1]=data[i][APTX];
  	 N1++;  x_1[N1]=data[i][SPOS];  y_1[N1]=0.;
  	}
  }
  N1++; x_1[N1]=data[N][SPOS]; y_1[N1]=0;
  //APTY
  N2=0; x_2[N2]=0; y_2[N2]=0;
  for( i=0; i<N; i++){
  	if( data[i][APTSTAT]==1 ) { //ENTRANCE aperture
  	 N2++;  x_2[N2]=data[i][SPOS];  y_2[N2]=0.;
  	 N2++;  x_2[N2]=data[i][SPOS];  y_2[N2]=-data[i][APTY];
  	}
  	if( data[i][APTSTAT]==2 ) { //EXIT aperture
  	 N2++;  x_2[N2]=data[i][SPOS];  y_2[N2]=-data[i][APTY];
  	 N2++;  x_2[N2]=data[i][SPOS];  y_2[N2]=0.;
  	}
  }
  N2++; x_2[N2]=data[N][SPOS]; y_2[N2]=0;

  TGraph *grAPTX = new TGraph(N1,x_1,y_1);
  TGraph *grAPTY = new TGraph(N2,x_2,y_2);
   //grAPTX->SetFillColor(kBlue-8);
   grAPTX->SetFillColorAlpha(kBlue-8, 0.4); //color, transparency
   grAPTY->SetFillColorAlpha(kRed-8, 0.4); //color, transparency

  //Apply factor to alternate apertures
  for( i=0; i<N1; i++) y_1f[i] = y_1[i]*apt_fact ;
  for( i=0; i<N1; i++) y_2f[i] = y_2[i]*apt_fact ;

  TGraph *grAPTXf = new TGraph(N1,x_1,y_1f);
  TGraph *grAPTYf = new TGraph(N2,x_2,y_2f);
   //grAPTXf->SetFillColor(kBlue-8);
   grAPTXf->SetFillColorAlpha(kBlue-8, 0.4); //color, transparency
   grAPTYf->SetFillColorAlpha(kRed-8, 0.4); //color, transparency



  Float_t xmin, xmax, y2Min=rMin, y2Max=rMax;
  xmin=data[0][SPOS]; xmax=xmin;
  for( i=0; i<N; i++){
    if( xmin>data[i][SPOS] ) xmin=data[i][SPOS];
    if( xmax<data[i][SPOS] ) xmax=data[i][SPOS];
  }
  Float_t xref[2], yref[2];
  xref[0]=xmin; xref[1]=xmax; yref[0]=0.; yref[1]=0.;


if( do_cPlotBmEnv )
{
// Create graphs
  Int_t Ngr=0;
  TGraph* gr[NC];
  TLegend* leg_gr = new TLegend(0.71,0.81,0.89,0.995);

  //create envelope plots
  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]=  data[i][PX] * 10; } //--NEW--
   gr[Ngr] = new TGraph(N, x, y);
   leg_gr->AddEntry(gr[Ngr],"Xm x 10","L");
   gr[Ngr]->SetLineColor(kBlue);
   gr[Ngr]->SetLineWidth(2);
   gr[Ngr]->SetLineStyle(1);
   Ngr++;
  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]= -data[i][PY] * 10; } //--NEW--
   gr[Ngr] = new TGraph(N, x, y);
   leg_gr->AddEntry(gr[Ngr],"Ym x 10","L");
   gr[Ngr]->SetLineColor(kRed);
   gr[Ngr]->SetLineWidth(2);
   gr[Ngr]->SetLineStyle(1);
   Ngr++;

  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]=  data[i][ME1_6]*fact_ME_16; }
   gr[Ngr] = new TGraph(N, x, y);
   sprintf(buffer,"(X,#delta_{E})*%g",fact_ME_16);
   leg_gr->AddEntry(gr[Ngr],buffer,"L");
   gr[Ngr]->SetLineColor(kBlack);
   gr[Ngr]->SetLineWidth(1);
   gr[Ngr]->SetLineStyle(2);
   Ngr++;

  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]=  10*data[i][PX_DXD]; }
   gr[Ngr] = new TGraph(N, x, y);
   leg_gr->AddEntry(gr[Ngr],"[Xm+|#delta*(X,#delta)|] x10","L"); //--NEW--
   gr[Ngr]->SetLineColor(kCyan);
   gr[Ngr]->SetLineWidth(2.0);
   gr[Ngr]->SetLineStyle(1);
   Ngr++;
   /*--NEW--
  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]= -data[i][PY_DYD]; }
   gr[Ngr] = new TGraph(N, x, y);
   leg_gr->AddEntry(gr[Ngr],"Ym+|#delta*(y,#delta)|","L");
   gr[Ngr]->SetLineColor(kRed);
   gr[Ngr]->SetLineWidth(2.0);
   gr[Ngr]->SetLineStyle(1);
   Ngr++;
   */
  /*Save data to file for inspection
  fp = fopen("tmp.txt", "w");
  for( i=0; i<N; i++){
  	sprintf(buffer," %f %f \n", x[i], y[i] );
  	fprintf(fp,buffer);
  }
  fclose(fp);*/


  // create graphs - beam envelopes
  if ( (TCanvas*)gROOT->FindObject("cPlotBmEnv") ) {
    printf("Refresh existing canvas.\n");
  } else {
    printf("Create new canvas.\n");
    TCanvas *cPlotBmEnv = new TCanvas("cPlotBmEnv","cPlotBmEnv: beam envelopes",40,40,600,400);
  }
  cPlotBmEnv->cd();
  TGraph *plot1ref = new TGraph(2,xref,yref);
  plot1ref->Draw("AL");
    plot1ref->GetXaxis()->SetLabelSize(0.04);
    plot1ref->GetYaxis()->SetLabelSize(0.04);
    plot1ref->GetYaxis()->SetRangeUser(rMin, rMax);
    plot1ref->GetXaxis()->SetRangeUser(xmin, xmax);
    plot1ref->SetTitle("TOP: Xm,  BOT.: -Ym; s[m]; [m]");    
// draw the graph with axis, continuous line, and put a * at each point
  //Axis and line
  grAPTX->Draw("F"); // A=axes
  grAPTY->Draw("F"); // A=axes
  //Graph envelopes
  //gr[4]->Draw("L");
  gr[3]->Draw("L");
  gr[2]->Draw("L");
  gr[1]->Draw("L");
  gr[0]->Draw("L");
  plot1ref->Draw("L");
  leg_gr->Draw();
  cPlotBmEnv->SaveAs("cPlotBmEnv.gif");
} // do_cPlotBmEnv


if( do_cPlotOrb )
{
// Reference trajectory Orbit plots
  Int_t NgrOrb=0;
  TGraph* grOrb[6];
  TLegend* leg_grOrb = new TLegend(0.78,0.88,0.89,0.995);
  //create orbit plots
  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]=  data[i][X0]; }
   grOrb[NgrOrb] = new TGraph(N, x, y);
   leg_grOrb->AddEntry(grOrb[NgrOrb],"X0","L");
   grOrb[NgrOrb]->SetLineColor(kBlue);
   grOrb[NgrOrb]->SetLineWidth(2);
   grOrb[NgrOrb]->SetLineStyle(1);
   NgrOrb++;
  for( i=0; i<N; i++){ x[i]=data[i][SPOS];   y[i]=  data[i][Y0]; }
   grOrb[NgrOrb] = new TGraph(N, x, y);
   leg_grOrb->AddEntry(grOrb[NgrOrb],"Y0","L");
   grOrb[NgrOrb]->SetLineColor(kRed);
   grOrb[NgrOrb]->SetLineWidth(2);
   grOrb[NgrOrb]->SetLineStyle(1);
   NgrOrb++;
  // Create canvas
  if ( (TCanvas*)gROOT->FindObject("cPlotOrb") ) {
    printf("Refresh existing canvas.\n");
  } else {
    printf("Create new canvas.\n");
    TCanvas *cPlotOrb = new TCanvas("cPlotOrb","cPlotOrb",80,80,600,400);
  }
  cPlotOrb->cd();
  TGraph *plotOrbRef = new TGraph(2,xref,yref);
  plotOrbRef->Draw("AL");
    plotOrbRef->GetXaxis()->SetLabelSize(0.04);
    plotOrbRef->GetYaxis()->SetLabelSize(0.04);
    plotOrbRef->GetYaxis()->SetRangeUser(rMin, rMax);
    plotOrbRef->GetXaxis()->SetRangeUser(xmin, xmax);
    plotOrbRef->SetTitle("Ref. trajectory orbit; s[m]; [m]");    
  //Axis and line
  grAPTX->Draw("F"); // A=axes
  grAPTY->Draw("F"); // A=axes
  //Graph envelopes
  grOrb[0]->Draw("L");
  grOrb[1]->Draw("L");
  plotOrbRef->Draw("L");
  leg_grOrb->Draw();
  cPlotOrb->SaveAs("cPlotOrb.gif");
  cPlotOrb->SaveAs("cPlotOrb.C");
} // do_cPlotOrb



if( do_cPlotAberr )
{
  //Assign index of column for each aberration to plot
  //pppppppppppppppppppppppppppppppppppppppppppppppppppp
  //create aberration plots: (1,o2) for select columns
  Int_t N_xo2;
    TGraph* gr_xo2[50];
    Int_t i_xo2[50];
    TLegend* leg_xo2 = new TLegend(0.7107023,0.8101604,0.8946488,0.9946524);
  N_xo2=0; //count them
  i_xo2[N_xo2] = 53-1; N_xo2++;
  i_xo2[N_xo2] = 48-1; N_xo2++;
  i_xo2[N_xo2] = 51-1; N_xo2++;
  i_xo2[N_xo2] = 54-1; N_xo2++;
  //for( j=0; j<N_xo2; j++) cout << Cnames[i_xo2[j]] << endl;
  //fill data into arrays and create TGraph for each indexed column
  for( j=0; j<N_xo2; j++){
  	for( i=0; i<N; i++){ x[i]=data[i][SPOS]; y[i]=data[i][i_xo2[j]]; }
  	x[0]=0; y[0]=0;
  	gr_xo2[j] = new TGraph(N, x, y);
  	//cout << j << "  " << i_xo2[j] << endl;
  }
  
  //create aberration plots: (3,o2) for select columns
  Int_t N_yo2;
    TGraph* gr_yo2[50];
    Int_t i_yo2[50];
    TLegend* leg_yo2 = new TLegend(0.7107023,0.8101604,0.8946488,0.9946524);
  N_yo2=0; //count them
  i_yo2[N_yo2] = 69-1; N_yo2++;
  i_yo2[N_yo2] = 67-1; N_yo2++;
  i_yo2[N_yo2] = 68-1; N_yo2++;
  i_yo2[N_yo2] = 65-1; N_yo2++;
  //for( j=0; j<N_yo2; j++) cout << Cnames[i_yo2[j]] << endl;
  //fill data into arrays and create TGraph for each indexed column
  for( j=0; j<N_yo2; j++){
  	for( i=0; i<N; i++){ x[i]=data[i][SPOS]; y[i]=data[i][i_yo2[j]]; }
  	x[0]=0; y[0]=0;
  	gr_yo2[j] = new TGraph(N, x, y);
  	//cout << j << "  " << i_yo2[j] << endl;
  }
  
  //create aberration plots: (1,o3) for select columns
  Int_t N_xo3;
    TGraph* gr_xo3[50];
    Int_t i_xo3[50];
    TLegend* leg_xo3 = new TLegend(0.7107023,0.7,0.8946488,0.9946524);
  N_xo3=0; //count them
  i_xo3[N_xo3] = 92-1; N_xo3++;
  i_xo3[N_xo3] = 86-1; N_xo3++;
  i_xo3[N_xo3] = 79-1; N_xo3++;
  i_xo3[N_xo3] = 93-1; N_xo3++;
  i_xo3[N_xo3] = 89-1; N_xo3++;
  i_xo3[N_xo3] = 94-1; N_xo3++;
  //for( j=0; j<N_xo3; j++) cout << Cnames[i_xo3[j]] << endl;
  //fill data into arrays and create TGraph for each indexed column
  for( j=0; j<N_xo3; j++){
  	for( i=0; i<N; i++){ x[i]=data[i][SPOS]; y[i]=data[i][i_xo3[j]]; }
  	x[0]=0; y[0]=0;
  	gr_xo3[j] = new TGraph(N, x, y);
  	//cout << j << "  " << i_xo3[j] << endl;
  }

  //create aberration plots: (3,o3) for select columns
  Int_t N_yo3;
    TGraph* gr_yo3[50];
    Int_t i_yo3[50];
    TLegend* leg_yo3 = new TLegend(0.7107023,0.7,0.8946488,0.9946524);
  N_yo3=0; //count them
  i_yo3[N_yo3] = 110-1; N_yo3++;
  i_yo3[N_yo3] = 107-1; N_yo3++;
  i_yo3[N_yo3] = 108-1; N_yo3++;
  i_yo3[N_yo3] = 109-1; N_yo3++;
  i_yo3[N_yo3] = 101-1; N_yo3++;
  //for( j=0; j<N_yo3; j++) cout << Cnames[i_yo3[j]] << endl;
  //fill data into arrays and create TGraph for each indexed column
  for( j=0; j<N_yo3; j++){
  	for( i=0; i<N; i++){ x[i]=data[i][SPOS]; y[i]=data[i][i_yo3[j]]; }
  	x[0]=0; y[0]=0;
  	gr_yo3[j] = new TGraph(N, x, y);
  	//cout << j << "  " << i_yo3[j] << endl;
  }
  //pppppppppppppppppppppppppppppppppppppppppppppppppppp END

  // Create canvas
  if ( (TCanvas*)gROOT->FindObject("cPlotAberr") ) {
    printf("Refresh existing canvas.\n");
  } else {
    printf("Create new canvas.\n");
    TCanvas *cPlotAberr = new TCanvas("cPlotAberr","cPlotAberr",120,120,1200,800);
    cPlotAberr->Divide(2,2); cPlotAberr->cd(1);
  }
  cPlotAberr->cd(1);
  TGraph *plotB1ref = new TGraph(2,xref,yref);
  plotB1ref->Draw("AL");
    plotB1ref->GetXaxis()->SetLabelSize(0.04);
    plotB1ref->GetYaxis()->SetLabelSize(0.04);
    plotB1ref->GetYaxis()->SetRangeUser(rMin*apt_fact, rMax*apt_fact);
    plotB1ref->GetXaxis()->SetRangeUser(xmin, xmax);
    sprintf(buffer,"x terms, order 2; s[m]; [m]");
    if( apt_fact!=1 ) {
      sprintf(tmp," ,  x%g on apertures",apt_fact);
      strcat(buffer,tmp) ;
    }
    plotB1ref->SetTitle(buffer);
  grAPTXf->Draw("F"); // A=axes
//1:  axis2->Draw();
  grAPTYf->Draw("F"); // A=axes
  gr_xo2[0]->Draw("L");
  for( i=1; i<N_xo2; i++){  gr_xo2[i]->Draw("L"); }
    for( i=0; i<N_xo2; i++) gr_xo2[i]->SetLineWidth(2);
    for( i=0; i<N_xo2; i++) gr_xo2[i]->SetLineStyle(1);
    for( i=0; i<N_xo2; i++) gr_xo2[i]->SetLineColor(i+1);
  for( i=0; i<N_xo2; i++){ 
    leg_xo2->AddEntry(gr_xo2[i], Cnames[i_xo2[i]], "L");
    leg_xo2->Draw();
  }
  plotB1ref->Draw("L");

  cPlotAberr->cd(2);
  TGraph *plotB2ref = new TGraph(2,xref,yref);
  plotB2ref->Draw("AL");
    plotB2ref->GetXaxis()->SetLabelSize(0.04);
    plotB2ref->GetYaxis()->SetLabelSize(0.04);
    plotB2ref->GetYaxis()->SetRangeUser(rMin*apt_fact, rMax*apt_fact);
    plotB2ref->GetXaxis()->SetRangeUser(xmin, xmax);
    sprintf(buffer,"y terms, order 2; s[m]; [m]");
    if( apt_fact!=1 ) {
      sprintf(tmp," ,  x%g on apertures",apt_fact);
      strcat(buffer,tmp) ;
    }
    plotB2ref->SetTitle(buffer);
  grAPTXf->Draw("F"); // A=axes
//1:  axis2->Draw();
  grAPTYf->Draw("F"); // A=axes
  gr_yo2[0]->Draw("L");
  for( i=1; i<N_yo2; i++){  gr_yo2[i]->Draw("L"); }
    for( i=0; i<N_yo2; i++) gr_yo2[i]->SetLineWidth(2);
    for( i=0; i<N_yo2; i++) gr_yo2[i]->SetLineStyle(1);
    for( i=0; i<N_yo2; i++) gr_yo2[i]->SetLineColor(i+1);
  for( i=0; i<N_yo2; i++){ 
    leg_yo2->AddEntry(gr_yo2[i], Cnames[i_yo2[i]], "L");
    leg_yo2->Draw();
  }
  plotB2ref->Draw("L");

  cPlotAberr->cd(3);
  TGraph *plotB3ref = new TGraph(2,xref,yref);
  plotB3ref->Draw("AL");
    plotB3ref->GetXaxis()->SetLabelSize(0.04);
    plotB3ref->GetYaxis()->SetLabelSize(0.04);
    plotB3ref->GetYaxis()->SetRangeUser(rMin*apt_fact, rMax*apt_fact);
    plotB3ref->GetXaxis()->SetRangeUser(xmin, xmax);
    sprintf(buffer,"x terms, order 3; s[m]; [m]");
    if( apt_fact!=1 ) {
      sprintf(tmp," ,  x%g on apertures",apt_fact);
      strcat(buffer,tmp) ;
    }
    plotB3ref->SetTitle(buffer);
  grAPTXf->Draw("F"); // A=axes
//1:  axis2->Draw();
  grAPTYf->Draw("F"); // A=axes
  gr_xo3[0]->Draw("L");
  for( i=1; i<N_xo3; i++){  gr_xo3[i]->Draw("L"); }
    for( i=0; i<N_xo3; i++) gr_xo3[i]->SetLineWidth(2);
    for( i=0; i<N_xo3; i++) gr_xo3[i]->SetLineStyle(1);
    for( i=0; i<N_xo3; i++) gr_xo3[i]->SetLineColor(i+1);
  for( i=0; i<N_xo3; i++){ 
    leg_xo3->AddEntry(gr_xo3[i], Cnames[i_xo3[i]], "L");
    leg_xo3->Draw();
  }
  plotB3ref->Draw("L");

  cPlotAberr->cd(4);
  TGraph *plotB4ref = new TGraph(2,xref,yref);
  plotB4ref->Draw("AL");
    plotB4ref->GetXaxis()->SetLabelSize(0.04);
    plotB4ref->GetYaxis()->SetLabelSize(0.04);
    plotB4ref->GetYaxis()->SetRangeUser(rMin*apt_fact, rMax*apt_fact);
    plotB4ref->GetXaxis()->SetRangeUser(xmin, xmax);
    sprintf(buffer,"y terms, order 3; s[m]; [m]");
    if( apt_fact!=1 ) {
      sprintf(tmp," ,  x%g on apertures",apt_fact);
      strcat(buffer,tmp) ;
    }
    plotB4ref->SetTitle(buffer);
  grAPTXf->Draw("F"); // A=axes
//1:  axis2->Draw();
  grAPTYf->Draw("F"); // A=axes
  gr_yo3[0]->Draw("L");
  for( i=1; i<N_yo3; i++){  gr_yo3[i]->Draw("L"); }
    for( i=0; i<N_yo3; i++) gr_yo3[i]->SetLineWidth(2);
    for( i=0; i<N_yo3; i++) gr_yo3[i]->SetLineStyle(1);
    for( i=0; i<N_yo3; i++) gr_yo3[i]->SetLineColor(i+1);
  for( i=0; i<N_yo3; i++){ 
    leg_yo3->AddEntry(gr_yo3[i], Cnames[i_yo3[i]], "L");
    leg_yo3->Draw();
  }
  plotB4ref->Draw("L");
  cPlotAberr->SaveAs("cPlotAberr.gif");
    // .gif ~80% smaller size than .png
    // .jpg ~80% smaller size than .gif
} // do_cPlotAberr



if( do_cRayPlots )
{
  printf(" do_cRayPlots: This section works for ROOT5 but gives\n errors in ROOT6. Section has been disabled.\n Aborting..."); exit(-1);
//---------------------- merge ray plots in pic###.ps  -------------------
  //Optional: Convert to pdf               
  system("ps2pdf pic001.ps");
  system("ps2pdf pic002.ps");

  TASImage* imgxs = new TASImage;
  imgxs->ReadImage("pic001.pdf"); //This call gives error in ROOT6
  Float_t xw = imgxs->GetWidth();
  Float_t yw = imgxs->GetHeight();
  //Magnification factors
  imgxs->Zoom(xw*.05,yw*.1,  xw*.88,yw*.75);
  cout << "imgxs xw= " << xw << " yw= " << yw << endl;
  TASImage* imgys = new TASImage; imgys->ReadImage("pic002.pdf");
  imgys->Zoom(xw*.05,yw*.1,  xw*.88,yw*.75);

  if ( (TCanvas*)gROOT->FindObject("cRayPlots") ) {
    cout << "refresh existing 'cRayPlots' "<<endl;
  } else {
    cout << "create new 'cRayPlots' "<<endl;
    TCanvas* cRayPlots = new TCanvas("cRayPlots","cRayPlots: x,y vs s",80,80,800,600);
  }
  cRayPlots->Clear();
  cRayPlots->Divide(1,2);
  cRayPlots->cd(1);  imgxs->Draw("x");
  cRayPlots->cd(2);  imgys->Draw("x");
  cRayPlots->SaveAs("cRayPlots.png");
} // do_cRayPlots



  printf("  NOTE: If any plots fail to show correctly, then run script again.\n");
}


//Hints on second axis
//https://root.cern.ch/root/html/tutorials/hist/transpad.C.html
//Other possible references
// http://www.desy.de/~gbrandt/root/mkraemer.pdf
//1: This method of secondary y axis is not practical in the future.
//1: Needs to be revisited in the future.
//1:  TGaxis *axis2 = new TGaxis(xmax,y2Min, xmax,y2Max, y2Min,y2Max, 50510,"+L");
//1:  axis2->SetLabelColor(kBlack);
