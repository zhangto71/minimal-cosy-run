{
/* ROOT6 compatible script
   - Convert .ps -> .pdf -> png
   - Arrange multiple pics in TCanvas
pdftoppm <pdf-file-convert> <output-file-name> -png 
 [x,y res.]          -rx <dpi-horizontal> -ry <dpi-vertical>
 [crop to left corner]          -x <hor pnt> -y <vert pnt>
 [width of crop area]           -W <width>  -H <height>
 [https://linux.die.net/man/1/pdftoppm]
%s.pdf %s
*/

  TStyle *myplain = new TStyle("myplain","myplain");
    myplain->SetCanvasColor(kGray);
    myplain->SetCanvasBorderMode(1);
    myplain->SetPadBorderMode(0);
    myplain->SetPadBorderSize(0);
    myplain->SetPadColor(0);
    myplain->SetPadLeftMargin(0.001);
    myplain->SetPadRightMargin(0.001);
    myplain->SetPadTopMargin(0.001);
    myplain->SetPadBottomMargin(0.001);
    myplain->SetTitleColor(0);
    myplain->SetStatColor(0);
  gROOT->SetStyle("myplain");

int cH=900, cV=700;
//cV=cH*1.083;
TCanvas *cPlots1 = new TCanvas("cPlots1", "cPlots1", 30,30, cH,cV);
cPlots1->Divide(1,2);
double xw, yw;
int k=0;
TImage *i1 = TImage::Open("cPlotBmEnv.gif");
TImage *i2 = TImage::Open("cScatPlot.png");
// xw = i1->GetWidth(), yw = i1->GetHeight(); cout<<xw<<" "<<yw<<endl;
// i1->Zoom(0.05*xw, 0.1*yw, 0.8*xw, 0.8*yw);
TPad *p1 = new TPad("i", "i", 0,0, 1,1); cPlots1->cd(1); i1->Draw();
TPad *p2 = new TPad("i", "i", 0,0, 1,1); cPlots1->cd(2); i2->Draw();

//cout<<"millisecond Pausing..."<<endl; gSystem->Sleep(2000);
//cPlots1->SaveAs("cPlots1.png");
//system("rm pic*-1.png");
return;
}
/*
TCanvas *c2 = new TCanvas("c2", "c2", 30,30, 200,200);
i1->Open("cScatPlot.png")
i1->Open("cPlotBmEnv.gif")
 i1->Draw()
 p1->Update()
 p1->Modified()
 p1->Update()
 
p1->SetGridx(); p1->Modified(); p1->Update();

*/