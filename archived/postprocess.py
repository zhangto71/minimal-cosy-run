#!/usr/bin/python3
# chmod +x postprocess.py; ./postprocess.py
#
# Scripts for post processing output from FIT processes.
# Changes:
#2022-Oct-20, MP: First version

__version__ = '1.0'

import os, shutil, sys, time

#------------------------------------------------------------
if __name__ == '__main__':
  print(" --- start: " + sys.argv[0])

  #- P P - P P - P P - P P - P P - P P - P P - P P - P P - P P - P P
  
  if False:
  # Post process - move scripts, generate .csv files, generate animations 
    print(" ---- move scripts ")
    os.system("cp -P -n ../aris-dev .")
    #os.system("cp -n aris-dev/tools.py .")
    #from tools import *
    os.system("cp -n aris-dev/rootapps/mpole_data_PROCESS_PS.C .")
    os.system("cp -n aris-dev/scale_and_refit_process_PS/mpole_data_PLOTS_v02.C .")
    os.system("cp -n aris-dev/scale_and_refit_process_PS/anim_ps_x4_TASImage.c .")
    print(" ---- deploy scripts ")
    os.system("root -l -q mpole_data_PROCESS_PS.C")
    os.system("root -l -b -q anim_ps_x4_TASImage.c")
    os.system("root -l -b -q mpole_data_PLOTS_v02.C")
    print(" ---- end. ")
    exit()
    
  if True:
  # Post process - move scripts, generate .csv files, generate animations 
    print(" ---- update plots  ")
    #os.system(" root -l -b -q rootapps/canvas_of_6_ps_files.c ")
    #os.system(" root -l -b -q COSY_WRAY_files_to_TTree_v4.c ")
    #os.system(" root -l -b -q rootapps/plot_TYPE_SLOG-v2.C ")
    os.system(" root -l -b -q plot_TYPE_SLOG-v2x10.C ")
    #os.system(" root -l mpole_data_PROCESS_PS.C ")
    print(" ---- end. ")
    exit()   

  #- P P - P P - P P - P P - P P - P P - P P - P P - P P - P P - end
else:
  exit()
