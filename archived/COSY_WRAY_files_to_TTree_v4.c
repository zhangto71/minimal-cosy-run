/* 2022-11, Scatter and TH2D histogram plots of COSY WRAY.* output
   Determine phase space params and plot ellipses
   portillo ... root_scripts\root\TOOLS_beam
*/

  // Conversion factor
  Float_t conv=1000.0; //m to mm
  double efact = 4.0 ; //factor to multiply emmitance by
  int extra_text_method=1 ;
  //limits
  double xH, xL, aH, aL;
  double yH, yL, bH, bL;
  double tH, tL, dH, dL;
  
  // Twiss parameter arrays for x-a, y-b, t-d
  // 0 E=emit   1 B=beta   2 G=gamma   3 A=alpha 
  double EBGAx[4], statx[7];
  double EBGAy[4], staty[7];
  double EBGAt[4], statt[7];
  double xc=0.0, ac=0.0, yc=0.0, bc=0.0, tc=0.0, dc=0.0; //default centroid values
  //Corresponding TEllipse params
  double TEllxa[3]; // th, rad1, rad2
  double TEllyb[3];
  double TElltd[3];
  const int NSB = 11;
  double SB[NSB]; //SB_params, PX PA R12 PY PB R34 PT PD R56 PG PZ
  double xrms, arms, covax, yrms, brms, covby, trms, drms, covdt;
  //other
  const double degrad = 0.017453293; //3.1415927/180.0;
  char tmps[200]; TLatex lcal;

 char buffer[256];
 const Int_t Np1=101; // Number of ellipse array points (best if odd)
 Float_t x1A[Np1], y1A[Np1]; //arrays for plotting
 Float_t x1_c[1], y1_c[1]; //centroid
 //Plot color order  1black, 2red, 3green, 4blue, 5yellow, 6magenta, 7lt-blue
 const Int_t Nsm=10;
 Int_t col1=1, colA[Nsm];

void TEll_sets(TEllipse *TE)
{
      TE->SetLineWidth(3); //1-default
      TE->SetLineColor(2); //1-black, 2-red
      TE->SetLineStyle(1); //1-solid 2-dashed 3-dotted ...
      TE->SetFillColor(0);//avoids black background in Legend
      TE->SetFillStyle(4000); //transparent fill
      //TE->Draw("L");
      return;
}

void hSets(TH2D *h)
{
  h->GetYaxis()->SetTitleOffset(1.35);
  return;
}

//void read_1d_array( char fname[]=(char*)"name.txt", int N=0, double *x,
//  int flag=1, int Nskip=0, char com[1]=(char*)"#" )
void read_1d_array( double *x, //not initialized 
  int N=1, 
  char fname[]=(char*)"name.txt", int flag=1, int Nskip=0, char com[1]=(char*)"#" )
{
  /* Read N values into array x
     Nskip - lines to skip before reading
     com   - character to interpret as comment line to skip
  */
  const int LLEN=1024; FILE *fp; char buff[LLEN];

  char *pch; int i=0;
  if( flag==0 ){
    printf(" ERROR!!! INVALID flag = %d \n", flag);
  } else if( flag==1 ) {
    fp = fopen( fname, "r");
    for( i=0; i<Nskip; i++) fgets( buff, LLEN, fp);
    for( i=0; i<N; i++) {
      fgets( buff, LLEN, fp);
      sscanf( buff, "%lf", &x[i] );
    }
    fclose( fp );
  } else {
    printf(" ERROR!!! read_1d_array ; INVALID flag = %d \n", flag);
  }
  return;
}

void scaleSB( double *SB, double scl=1000.)
{ //scale COSY SB phase space parameters, e.g. to convert from mm to m
  SB[0] *= scl; SB[1] *= scl;
  SB[3] *= scl; SB[4] *= scl;
  SB[6] *= scl; SB[7] *= scl;
  return;
}

void convSBtoTwiss( double *SB, double *EBGAx, double *EBGAy, double *EBGAt )
{
  //emit (eps)
  EBGAx[0] = SB[0]*SB[1]*(1-SB[2]*SB[2]);
  EBGAy[0] = SB[3]*SB[4]*(1-SB[5]*SB[5]);
  EBGAt[0] = SB[6]*SB[7]*(1-SB[8]*SB[8]);
  // beta
  EBGAx[1] = SB[0]*SB[0]/EBGAx[0] ;
  EBGAy[1] = SB[3]*SB[3]/EBGAy[0] ;
  EBGAt[1] = SB[6]*SB[6]/EBGAt[0] ;
  // gamma
  EBGAx[2] = SB[1]*SB[1]/EBGAx[0] ;
  EBGAy[2] = SB[4]*SB[4]/EBGAy[0] ;
  EBGAt[2] = SB[7]*SB[7]/EBGAt[0] ;
  // alpha
  EBGAx[3] = -1.0*SB[2]*sqrt(EBGAx[1]*EBGAx[2]) ;
  EBGAy[3] = -1.0*SB[5]*sqrt(EBGAy[1]*EBGAy[2]) ;
  EBGAt[3] = -1.0*SB[8]*sqrt(EBGAt[1]*EBGAt[2]) ;
  printf(" EBGAx %lf  %lf  %lf  %lf\n", EBGAx[0], EBGAx[1], EBGAx[2], EBGAx[3]);
  printf(" EBGAy %lf  %lf  %lf  %lf\n", EBGAy[0], EBGAy[1], EBGAy[2], EBGAy[3]);
  printf(" EBGAt %lf  %lf  %lf  %lf\n", EBGAt[0], EBGAt[1], EBGAt[2], EBGAt[3]);
  return;
}

void convTwissToTEllipse(double *EBGA, double *TEll)
{
 // TEll { xc, yc, th, rad1, rad2 }
  double th, rad1, rad2, t,s,c;
  // th = 0.5*atan(2*alpha1/(gam1-bet1)) ;
  th = 0.5*atan(2*EBGA[3]/(EBGA[2]-EBGA[1])) ;
  t = pow(tan(th),2); s = pow(sin(th),2) ; c = pow(cos(th),2) ;
  rad1 = sqrt(EBGA[0]*(t*EBGA[2]-EBGA[1])/(s*t-c)) ;
  rad2 = sqrt(EBGA[0]*(t*EBGA[1]-EBGA[2])/(s*t-c)) ;
  TEll[0] = th/0.017453293;
  TEll[1] = rad1;
  TEll[2] = rad2;
  printf(" --- convTwissToTEllipse ---:\n");
  printf(" EBGA %lf, %lf, %lf, %lf \n", EBGA[0], EBGA[1], EBGA[2], EBGA[3]);
  printf(" th %lf, rad1 %lf, rad2 %lf \n", th, rad1, rad2);
  return;
}

void dummy_graph( char title[]=(char*)"_;X;Y",
  double xlo=-1, double xhi=1, double ylo=-1, double yhi=1)
{ // xlo,xhi, ylo,yhi
  double xlim[4] = {xlo, xlo, xhi, xhi};
  double ylim[4] = {ylo, yhi, yhi, ylo};
  TGraph *gL = new TGraph(4,xlim,ylim);
  gL->SetTitle(title);
  gL->SetMarkerStyle(0);
  gL->SetMarkerColor(0);
  gL->Draw("AP");
  return;
}


//------------------------------------------------------------------------
//======================= main ===========================================
//------------------------------------------------------------------------
void COSY_WRAY_files_to_TTree_v4()
{
  gROOT->Reset();
  char str[512];
  sprintf(str,"rm COSY_WRAY.txt; sleep 2"); system(str);
  //sprintf(str,"cat WRAY.* > COSY_WRAY.txt"); system(str);
  sprintf(str,"grep -v -h '^                    NaN'  WRAY.* > COSY_WRAY.txt"); system(str);
  //
  TFile *f = new TFile("COSY_WRAY.root","RECREATE");
  TTree *T = new TTree("ntuple","data from ascii file");
  Int_t nRead = T->ReadFile( "COSY_WRAY.txt","xf:af:yf:bf:tf:df");
  Long64_t lln, NEntries = T->GetEntries();
  //Define Aliases and list them
  T->Write();
  //T->StartViewer(); //start window based analysis session
  
  //Set brach addresses to tree values
  Float_t xf,af,yf,bf,tf,df;
  T->SetBranchAddress("xf",&xf);
  T->SetBranchAddress("af",&af);
  T->SetBranchAddress("yf",&yf);
  T->SetBranchAddress("bf",&bf);
  T->SetBranchAddress("tf",&tf);
  T->SetBranchAddress("df",&df);

  //Aliases
  T->SetAlias("x_mm","xf*1000");
  T->SetAlias("a_mr","af*1000");
  T->SetAlias("y_mm","yf*1000");
  T->SetAlias("b_mr","bf*1000");
  T->SetAlias("dP","df*1");
  //Optionals:
  //f->Close(); 

  //LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  //LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  //limits
  //xH=5; aH=30;    yH=5; bH=30;    tH=1.1; dH=0.1;    //targ
  //xH=200; aH=30;    yH=4; bH=30;    tH=1.1; dH=0.1;    //PS wedge
  //xH=100; aH=100;    yH=10; bH=100;    tH=3; dH=0.2;    //PS fp (achro)
  //xH=300; aH=100;    yH=10; bH=100;    tH=3; dH=0.2;    //PS fp - (not acrho)
  xH=20; aH=50;    yH=20; bH=50;    tH=3; dH=0.2;    //PS fp - prim Xe
  //LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL
  //LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL

  xL=-xH; aL=-aH;  yL=-yH; bL=-bH;  tL=-tH; dL=-dH;
  Int_t NH=40, NV=40;  //bins

  TH2D *hax = new TH2D("hax","hax", NH,xL,xH, NV,aL,aH); hSets(hax);
  TH2D *hby = new TH2D("hby","hby", NH,yL,yH, NV,bL,bH); hSets(hby);
  TH2D *hdt = new TH2D("hdt","hdt", NH,tL,tH, NV,dL,dH); hSets(hdt);
  TH2D *hyx = new TH2D("hyx","hyx", NH,xL,xH, NV,yL,yH); hSets(hyx);
  TH2D *hxd = new TH2D("hxd","hxd", NH,xL,xH, NV,dL,dH); hSets(hxd);
  T->Draw("y_mm:x_mm>>hyx"); hyx->SetTitle("hyx;x [mm];y [mm]");
  T->Draw("a_mr:x_mm>>hax"); hax->SetTitle("hax;x [mm];a [mrad]");
  T->Draw("b_mr:y_mm>>hby"); hby->SetTitle("hby;y [mm];b [mrad]");
  T->Draw("df:x_mm>>hxd"); hxd->SetTitle("hxd;x [mm];dE/E");
  T->Draw("df:tf>>hdt"); hdt->SetTitle("hdt;t [m];dE/E");
  hax->GetStats(statx);
  hby->GetStats(staty);
  hdt->GetStats(statt);
  for( int i=1 ; i<7; i++) {
      statx[i] = statx[i]/statx[0] ;
      staty[i] = staty[i]/staty[0] ;
      statt[i] = statt[i]/statt[0] ;
      printf(" stat[%d] = x %lf,  y %lf,  t %lf\n", i, statx[i], staty[i], statt[i] );  
  }
  xc = statx[2]; ac = statx[4];
  yc = staty[2]; bc = staty[4];
  tc = statt[2]; dc = statt[4];
  xrms=sqrt(statx[3]-xc*xc); arms=sqrt(statx[5]-ac*ac); covax=statx[6]-xc*ac;
  yrms=sqrt(staty[3]-yc*yc); brms=sqrt(staty[5]-bc*bc); covby=staty[6]-yc*bc;
  trms=sqrt(statt[3]-tc*tc); drms=sqrt(statt[5]-dc*dc); covdt=statt[6]-tc*dc;
  
  // 0 E=emit   1 B=beta   2 G=gamma   3 A=alpha
  EBGAx[0] = sqrt(xrms*xrms*arms*arms-covax);
  EBGAy[0] = sqrt(yrms*yrms*brms*brms-covby);
  EBGAt[0] = sqrt(drms*drms*trms*trms-covdt);
  EBGAx[1]=xrms*xrms/EBGAx[0]; EBGAx[2]=arms*arms/EBGAx[0]; EBGAx[3]=-covax/EBGAx[0];
  EBGAy[1]=yrms*yrms/EBGAy[0]; EBGAy[2]=brms*brms/EBGAy[0]; EBGAy[3]=-covby/EBGAy[0];
  EBGAt[1]=trms*trms/EBGAt[0]; EBGAt[2]=drms*drms/EBGAt[0]; EBGAt[3]=-covdt/EBGAt[0];
  
  // Apply emit factor and convert Twiss param data to TEllipse array
  EBGAx[0] *= efact; EBGAy[0] *= efact; EBGAt[0] *= efact;
  convTwissToTEllipse(EBGAx, TEllxa);
  convTwissToTEllipse(EBGAy, TEllyb);
  convTwissToTEllipse(EBGAt, TElltd);
  // generate TEllipse objects for each phase space
  // TEllipse( xc,yc, r1,r2, phimin,phimax, theta )
  printf(" xc %lf, ac %lf\n", xc,ac);
  printf(" yc %lf, bc %lf\n", yc,bc);
  printf(" tc %lf, dc %lf\n", tc,dc);
  TEllipse *TEx = new TEllipse(xc,ac, TEllxa[1], TEllxa[2], 0.,360., TEllxa[0]);
  TEllipse *TEy = new TEllipse(yc,bc, TEllyb[1], TEllyb[2], 0.,360., TEllyb[0]);
  TEllipse *TEt = new TEllipse(tc,dc, TElltd[1], TElltd[2], 0.,360., TElltd[0]);
  TEll_sets(TEx); TEll_sets(TEy); TEll_sets(TEt);

  lcal.SetTextFont(42);
  lcal.SetTextAlign(33);
  lcal.SetTextSize(0.04);
  char ptype[]="col"; // "",".", "col", ...
  cout << "plot type, ptype= "<<ptype<<endl;
  
  int outtype = 2; //0-no output,  1-Twiss_data,   2-rms_data
  char form1[]="#splitline{eps*%2.1lf= %4.3le}{ beta= %4.3le, alpha= %4.3le}";
  char form2[]="#splitline{eps*%2.1lf= %4.3le, covHV= %4.3le}{ H_rms= %4.3le, V_rms= %4.3le}";

 int do_cPlotTH2D = 1 ;
 if( do_cPlotTH2D==1 )
 { // --------------------------- do_cPlotTH2D
   //Statistical emittance ellipse and data
   //Create canvas and plot points in array(s)
   gROOT->SetStyle("Plain");
   gStyle->SetPalette(1);
   gStyle->SetOptStat(kTRUE); //stat box show for all
   TCanvas *cPlotTH2D = new TCanvas("cPlotTH2D","cPlotTH2D",80,80, 1200,450);
   cPlotTH2D->Divide(3,1);
   cPlotTH2D->cd(1);
      //gStyle->SetOptStat(kFALSE);
      hax->Draw(ptype);
      TEx->Draw("L");
      sprintf( tmps, form1, efact, EBGAx[0], EBGAx[1], EBGAx[3]);
      lcal.DrawLatex(xH*0.6,aH*1.2,tmps);            
   cPlotTH2D->cd(2);
      //gStyle->SetOptStat(kFALSE);
      hby->Draw(ptype);
      TEy->Draw("L");
      sprintf( tmps, form1, efact, EBGAy[0], EBGAy[1], EBGAy[3]);
      lcal.DrawLatex(yH*0.6,bH*1.2,tmps);
   cPlotTH2D->cd(3);
      //gStyle->SetOptStat(kTRUE); //does not function
   /* Opt1: dE vs t
      hdt->Draw(ptype);
      TEt->Draw("L");
      sprintf( tmps, form1, efact, EBGAt[0], EBGAt[1], EBGAt[3]);
      lcal.DrawLatex(tH*0.6,dH*1.2,tmps);
      */
   // Opt2: dE vs t
      hxd->Draw(ptype);
      //hyx->Draw(ptype);
   cPlotTH2D->SaveAs("cPlotTH2D.png");
 } else {
   printf(" Invalid option do_cPlotTH2D=%d\n", do_cPlotTH2D);
 } // --------------------------- do_cPlotTH2D

 
 int do_cScatPlot = 1 ;
 Style_t ms=6;
 Color_t mc=1;
 // --------------------------- do_cScatPlot
 if( do_cScatPlot==0 )
 { 
   printf(" Invalid do_cScatPlot=%d\n", do_cScatPlot);
 } else if( do_cScatPlot==1 ){
   gROOT->SetStyle("Plain");
   gStyle->SetPalette(1);
   gStyle->SetOptStat(kTRUE); //stat box show for all
   TCanvas *cScatPlot = new TCanvas("cScatPlot","cScatPlot",120,120, 1200,450);
   cScatPlot->Divide(3,1);
   cScatPlot->cd(1);
    dummy_graph((char*)"; x_mm; a_mr", xL,xH, aL,aH);
    T->Draw("a_mr:x_mm","","same");  //generate TGraph of variables
     TGraph *grxa=(TGraph*)cScatPlot->cd(1)->GetListOfPrimitives()->FindObject("Graph");
     grxa->SetMarkerStyle(ms);
    // Draw ellipse and print Twiss params
    TEx->Draw("L");
    if (outtype==1){ // Show Twiss param data
      sprintf( tmps, form1, efact, EBGAx[0], EBGAx[1], EBGAx[3]);
      lcal.DrawLatex(xH*0.6,aH*1.45,tmps);
    } else if (outtype==2) { // SHow rms data
      sprintf( tmps, form2, efact, EBGAx[0], covax, xrms, arms);
      lcal.DrawLatex(xH*0.65,aH*1.45,tmps);
    }
    //
   cScatPlot->cd(2);
    dummy_graph((char*)"; y_mm; b_mr", yL,yH, bL,bH);
    T->Draw("b_mr:y_mm","","same");  //generate TGraph of variables
     TGraph *gryb=(TGraph*)cScatPlot->cd(2)->GetListOfPrimitives()->FindObject("Graph");
     gryb->SetMarkerStyle(ms);
    // Draw ellipse and print Twiss params
    TEy->Draw("L");
    if (outtype==1){ // Show Twiss param data
      sprintf( tmps, form1, efact, EBGAy[0], EBGAy[1], EBGAy[3]);
      lcal.DrawLatex(yH*0.6,bH*1.45,tmps);
    } else if (outtype==2) { // SHow rms data
      sprintf( tmps, form2, efact, EBGAy[0], covby, yrms, brms);
      lcal.DrawLatex(yH*0.65,bH*1.45,tmps);
    }
    //
   cScatPlot->cd(3);
   /* Opt1: dE vs t
    dummy_graph((char*)" ; df; td", dL,dH, tL,tH);
    T->Draw("tf:df","","same");  //generate TGraph of variables
     TGraph *grdt=(TGraph*)cScatPlot->cd(3)->GetListOfPrimitives()->FindObject("Graph");
     grdt->SetMarkerStyle(ms);
    // Draw ellipse and print Twiss params
    TEt->Draw("L");
    sprintf( tmps, form1, efact, EBGAt[0], EBGAt[1], EBGAt[3]);
    lcal.DrawLatex(dH*0.6,tH*1.45,tmps);
    */
    /* Opt2: dE vs x
    dummy_graph((char*)"; x_mm; dE/E", xL,xH, dL,dH);
    T->Draw("df:x_mm","","same");  //generate TGraph of variables
     TGraph *grdt=(TGraph*)cScatPlot->cd(3)->GetListOfPrimitives()->FindObject("Graph");
     grdt->SetMarkerStyle(ms);
    if (outtype==1){ // Show Twiss param data
    } else if (outtype==2) { // Show rms data
      sprintf( tmps, "dE/E_rms= %4.3le", drms);
      lcal.DrawLatex(xH*0.65,dH*1.45,tmps);
    } */
    // Opt3: y vs x
    dummy_graph((char*)"; x_mm; y_mm", xL,xH, yL,yH);
    T->Draw("y_mm:x_mm","","same");  //generate TGraph of variables
     TGraph *grdt=(TGraph*)cScatPlot->cd(3)->GetListOfPrimitives()->FindObject("Graph");
     grdt->SetMarkerStyle(ms);
    //
    cScatPlot->SaveAs("cScatPlot.png");
    //
 } else if( do_cScatPlot==2 ){
   gROOT->SetStyle("Plain");
   gStyle->SetPalette(1);
   gStyle->SetOptStat(kTRUE); //stat box show for all
   TCanvas *cScatPlot = new TCanvas("cScatPlot","cScatPlot",120,120, 1200,450);
   cScatPlot->Divide(3,1);
   cScatPlot->cd(1); T->Draw("a_mr:x_mm");
   cScatPlot->cd(2); T->Draw("b_mr:y_mm");
   cScatPlot->cd(3); T->Draw("df:tf");
   //
 } else if( do_cScatPlot==3 ){
    TCanvas *cs1 = new TCanvas("cs1","cs1", 10,10, 500,480);
    T->Draw("y_mm:x_mm");  //generate TGraph of variables
    //
 } else if( do_cScatPlot==4 ){
    TCanvas *cs1 = new TCanvas("cs1","cs1", 10,10, 500,480);
    T->Draw("a_mr:x_mm");  //generate 
    // Draw ellipse and print Twiss params
    TEx->Draw("L");
    sprintf( tmps, form1, efact, EBGAx[0], EBGAx[1], EBGAx[3]);
    lcal.DrawLatex(xH*0.6,aH*1.2,tmps); //not working
 } else if( do_cScatPlot==5 ){
    TCanvas *cs1 = new TCanvas("cs1","cs1", 10,10, 500,480);
    //dummy_graph();
    dummy_graph((char*)" ; x_mm; a_mr",xL,xH, aL,aH);
    T->Draw("a_mr:x_mm","","same");  //generate TGraph of variables
     TGraph *gryx=(TGraph*)cs1->cd(1)->GetListOfPrimitives()->FindObject("Graph");
     gryx->SetMarkerStyle(ms);
    // Draw ellipse and print Twiss params
    TEx->Draw("L");
    sprintf( tmps, form1, efact, EBGAx[0], EBGAx[1], EBGAx[3]);
    lcal.DrawLatex(xH*0.6,aH*1.45,tmps);
 } else {
   printf(" Invalid do_cScatPlot=%d\n", do_cScatPlot);
 } // --------------------------- do_cScatPlot

  //--print optional limits 
  printf(" --- rms values and optional limits ---:\n");
  printf(" xrms %lf, arms %lf\n", xrms,arms);
  printf(" yrms %lf, brms %lf\n", yrms,brms);
  printf(" trms %lf, drms %lf\n", trms,drms);
  printf("  xH=%lf; aH=%lf;\n", xrms*efact*2.0, arms*efact*2.0);
  printf("  yH=%lf; bH=%lf;\n", yrms*efact*2.0, brms*efact*2.0);
  printf("  tH=%lf; dH=%lf;\n", trms*efact*2.0, drms*efact*2.0);
  return;
}


/* COSY tools needed for this method to be used

  {-- SR based methods -- SR based methods -- SR based methods -- }
   PROCEDURE WRAY_COL IU ;   {PRINTS RAY IN COLUMN FORM TO UNIT IU
     THE FIRST RAY IS THE REFERENCE
     Output format is easier to read by external apps.
     Example: COSY_WRAY_files_to_TTree.C is used to read all files that are 
      generated with this and called WRAY.??? }
      VARIABLE NR 1 ; VARIABLE LABL 1 8 ; VARIABLE I 1 ; VARIABLE J 1 ;
      NR := LENGTH(RAY(1)) ; IF NR=1 ; NR := 0 ; ENDIF ; {SEE CR AND SR}
      {WRITE IU '# number of rays:'&SF(NR,'(I8)') ;}
      IF NR>0 ;
         {WRITE IU '#X A Y B T D' ; leave out G Z }
         LOOP J 3 NR ; {skip reference particles}
           WRITE IU S(RAY(1)|J)&' '&S(RAY(2)|J)&' '&S(
           RAY(3)|J)&' '&S(RAY(4)|J)&' '&S(RAY(5)|J)&' '&S(RAY(6)|J) ;
         ENDLOOP ;
      ENDIF ;
      ENDPROCEDURE ;

*/