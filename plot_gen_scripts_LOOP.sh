#!/bin/bash
for ((c=1; c<10000; c++))
do
	echo "Root script iteration" $c
	clear; 
	root -l -b -q plot_TYPE_SLOG-v2x10.C
	root -l -b -q COSY_WRAY_files_to_TTree_v4.c
	sleep 10
done
echo "...end loop.sh"