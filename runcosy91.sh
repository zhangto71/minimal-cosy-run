#!/bin/bash
#Updated 2021-06
# chmod +x runcosy91.sh; runcosy91.sh myfile.fox  (example commands for use)
# 
# This script allows the .fox file in the current directory to run with cosy 
#   executable files that reside in a different directory.
#  Advantages:
#  - The generated output will be put in the current directory.
#  - Checks if Linux environment is 64bit. Normally, 64bit is supported now,
#    but a compiled 32bit version of COSY can be used, as well.
#    (32 bit options is not available at this time)
#  - Generated symbolic links to avoid having to compile a local .bin file
#
#  - To avoid using this script, define the following path
#   export PATH="/projects/aris/cosy/cosy91deb10/bin:$PATH"
#    and use the following INCLUDE line in your cosy script
#   INCLUDE '/projects/aris/cosy/cosy91deb10/bin/COSY91';
#
start_time=$(date) ;
#-----define the directory where the cosy executable resides-------
fox_file=$1 ; # set variable $fox_file to fist input after command
if [ $HOSTTYPE = 'x86_64' ]; 
then
 #cosypath="/projects/aris/cosy/cosy91deb10" ;
 #cosypath="/projects/aris/cosy/cosy_stash" ;
 cosypath="cosy" ; #soft link
else
 cosypath="" ;
 echo "Currently, only 64bit supported. Aborting..."
 exit;
fi
echo $cosypath

#Use the following section only if .bin file will be used from source directory
#-------------prepare soft link to .bin file
COSYBIN='COSY91.bin' ; lnCOSYBIN=ln$COSYBIN ;
if [ ! -f $lnCOSYBIN ]; then
  echo "Creating symbolic link $lnCOSYBIN to $COSYBIN" ;
  ln -s $cosypath/$COSYBIN $lnCOSYBIN
fi

#--- In case SYSCA.DAT file is not included. Note that a custom version can 
#  be used instead of this one, if desired.
if [ ! -f 'SYSCA.DAT' ]; then
  #echo "Creating symbolic link to SYSCA.DAT file" ;
  #ln -s $cosypath/SYSCA.DAT SYSCA.DAT
  echo "Copying SYSCA.DAT file" ;
  cp $cosypath/SYSCA.DAT SYSCA.DAT
fi

#-------------prepare foxyinp.dat file for cosy to read-------------
#### Call cosy ######################################################
no_fox=$( echo $fox_file | sed 's/.fox//' ) ;
foxyinp='foxyinp.dat' ; rm -f $foxyinp ; echo 'new' >> $foxyinp ;
echo 'update... $foxyinp' ;
sed  '1i '"$no_fox"'' $foxyinp > rctmp ;
mv rctmp $foxyinp ;

#Run command to execute cosy with .fox file as input
echo $cosypath/cosy ;
$cosypath/cosy
#-- Below are optional calls that may be used for long computation times
#nohup nice $cosypath/cosy &
#nice $cosypath/cosy

#----------------------post cosy processing-------------------------------
#-- Section below is optional. It can be used for plotting SLOG output.
#sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
make_splots='splots' ;    # =1 process SLOG output file and plot using gnuplot
#if [ $2 = 'splots' ] ; then
if [ $make_splots = 'splots' ] ; then
  chmod +x process_SLOG.sh ; process_SLOG.sh
fi
#ssssssssssssssssssssssssssssssssssssssssssssssssssssssssss END

stop_time=$(date)
echo 'start: '$start_time
echo 'stop:  '$stop_time
