/*
Process data fitted over range of Brhos and generate .csv file for controls to 
  read gradiets (or fields) to use for scaling.

2021-11-30, MP: Debug issue associated with writing header lines.
2022-03-29, MP: Change format of gradient file to include CHIM at top.
2022-08-23, MP:
   1) modifications to run at single CHIM and process both GRAD and AMP values
   2) modifications to use another translator and allow select output names
2022-10-13, MP: Modification to read CHIM and CHIM2 (after wedge)

  --  Example format of COSY gradient field .dat file --
CHIM [T.m]
  4.000000000000000   
FSQ1 GRAD_R=  0.1040000000000000
2, 0.2911238746909701
FSQ2_S1 GRAD_R=  0.1300000000000000
2,-0.3923378828104478
3,-0.1166173804443559
4,-0.1036537758155080
FSQ2_S2 GRAD_R=  0.1300000000000000
2, 0.2697765415191592
3, 0.1490540780061987
4, 0.2190365652972731
FSD1_MSS_1 BEND_R=   4.000000000000000
1, 0.6250000000000562E-01
*/

//Name of file with translation names between COSY files and controls (or other)
//char ftrans[] ="/departments/AcceleratorPhysics/ARIS/Optics/aris-dev-portillo/param_files/name_translation_key_v2.csv";
char ftrans[] ="aris-dev/param_files/name_translation_key_v2.csv";
//char ftrans[] ="name_translation_key_v2.csv";

char fname_root[] = "%sCHIM_%06.4lf_GRAD.dat";

bool trans_names = 1; //if 1, then translate names in the file
int out_name_opt = 3; /*  1 = controls name
                          2 = .fox variable name
                          3 = section + controls  */

//Name of directory with field data to process
char dir_data[]="";
// file with header lines
char fheaders[]= "aris-dev/optics_files/header_template.csv";

const int LLEN=2048;
const int NLEN=128;
const int NCHI=100;   //max number of beam rigidity values
const int NMULTA=500;  //max number of multipole elements
double CHIM[NCHI];
double CHIM2[NCHI];
char fname_arr[NCHI][NLEN];
  char name_arr[NMULTA][NLEN];
  int n_arr[NMULTA];
  double R_arr[NMULTA];
  double fld_arr[NMULTA][NCHI];
  int i_CHI;
  int i_fld, Nfld=0;
  int i_Nmult, Nmult=0;

const int NNAMES=500;  //max number of names to translate
  char tr_sec[NNAMES][NLEN]; //section_name
  char tr_type[NNAMES][NLEN];
  char tr_name1[NNAMES][NLEN]; //name1
  char tr_name2[NNAMES][NLEN];
  char tr_name3[NNAMES][NLEN];
  char tr_name4[NNAMES][NLEN]; //section_name:name1 
  int  tr_n_arr[NNAMES];
  int Ntr; //number of names read from file for translation
double CHIM_0, CHIM_F, d_CHI ;
double CHIM_read[NNAMES] ;
bool debug1=0 ;

void generate_fname_arr_GRAD_CHIM_by_string(   char subd[50] )
{
  FILE *fp1;
  char fname[256];
  i_CHI = 0;
  for( double mCHI=CHIM_0; mCHI<=CHIM_F; mCHI+=d_CHI ){
    sprintf( fname_arr[i_CHI], fname_root, subd, mCHI );
    fp1 = fopen( fname_arr[i_CHI],"r");
    if(fp1==NULL){
      printf(" Skipping file not found: %s\n", fname_arr[i_CHI] );
    } else {
      CHIM[i_CHI] = mCHI;
      printf(" CHIM[%d]=%lf, %s\n", i_CHI, CHIM[i_CHI], fname_arr[i_CHI]);
      i_CHI++;
      fclose(fp1);
    }
  }
  Nfld = i_CHI;
  printf(" Total field values read from file, Nfld = %d\n", Nfld);
  return;
}

void read_multi_COSY_OUTPUT_files()
{
  // Read COSY output GRADient files from array of names. 
  //  Single element array for only one file.
  printf(" ---- read_multi_COSY_OUTPUT_files:\n");
  FILE *fp1;
  char buff1[LLEN];
  char *pch;
  char s_name1[LLEN]="elem_base_name";
  char s_name2[16]="n_multipole", s_dum1[64]="dummy";
  int i_n;
  double d_dum1, d_R;
  bool do_print;

  for( i_CHI=0; i_CHI<Nfld; i_CHI++ ){  //i_CHI sssssssss scan over files
    printf(" reading file %d = %s\n", i_CHI, fname_arr[i_CHI] );
    fp1 = fopen(fname_arr[i_CHI],"r");
    if( fp1==NULL ) { printf("  ERROR!!!, No file: %s\n", fname_arr[i_CHI]); exit(0); }
//    if(i_CHI<Nfld){
    //0000000000000000000000000000000000000000
     do_print=0;
     if(i_CHI==0) do_print=1;
     //---
     i_Nmult = 0; // read through each multipole
     while( !feof(fp1) ){
       if( fgets( buff1, LLEN, fp1)!=NULL ) {
        if(strncmp(buff1,"PS01CHIM",8)==0) {
          //CHIM1 read
          fgets( buff1, LLEN, fp1);
          sscanf( buff1, "%lf\n", &CHIM[i_CHI] ) ;
          if(do_print) printf(" read PS01CHIM[%d]= %lf\n", i_CHI, CHIM[i_CHI] );
        } else if(strncmp(buff1,"PS02CHIM",8)==0) {
          //CHIM2 read
          fgets( buff1, LLEN, fp1);
          sscanf( buff1, "%lf\n", &CHIM2[i_CHI] ) ;
          if(do_print) printf(" read PS02CHIM[%d]= %lf\n", i_CHI, CHIM2[i_CHI] );
        } else if( sscanf(buff1,"%d, %lf\n", &i_n, &d_dum1)!=2 ) {
        // Assume string = element title, GRAD_R, Leff
          pch=NULL;
          //extract title string
          pch = strtok( buff1," ");
           if(pch!=NULL){ sscanf(pch,"%s", s_name1); }
           //else{ printf("Invalid title. Abort.\n") ; exit(0) ; }
           else{ printf("Invalid title. Break.\n") ; }
           if(debug1) printf("title: %s\n",s_name1);
          //extract dummy string
          pch = strtok( NULL," ");
           if(pch!=NULL){ sscanf(pch,"%s", s_dum1); }
           else{ printf("Invalid dummy. Break.\n") ; }
           //printf("dummy: %s\n",s_dum1);
          //extract radius [m]
          pch = strtok( NULL," ");
           if(pch!=NULL){ sscanf(pch,"%lf", &d_dum1); }
           else{ printf("Invalid R. Break.\n") ; }
           d_R = d_dum1;
           //printf("R: %lf\n",d_dum1);
           //
           if(do_print) printf(" title= %s, R= %lf \n", s_name1, d_R );
        } else {
//          if( sscanf(buff1,"%d, %lf\n", &i_n, &d_dum1)!=2 ){
        // Assume string = multipole n, field (or field gradient)
          sprintf( name_arr[i_Nmult], "%s", s_name1 );
          R_arr[i_Nmult] = d_R ;
          n_arr[i_Nmult] = i_n;
          fld_arr[i_Nmult][i_CHI] = d_dum1;
          if(do_print) printf("i_Nmult %d: multipole n %d, fld %lf\n", 
            i_Nmult, n_arr[i_Nmult], fld_arr[i_Nmult][i_CHI] );
          i_Nmult++;
        }
       }
     }//while
     fclose(fp1);
     Nmult = i_Nmult;
     printf(" Total multipoles read from file, Nmult = %d\n", Nmult);
     
/*
    //0000000000000000000000000000000000000000 END
    } else {
      
    //nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
       do_print=0;
       //NNNNNNN Read next set of multipole fields
       i_Nmult = 0; // read through each multipole
       while( !feof(fp1) ){
         if( fgets( buff1, LLEN, fp1)!=NULL ) {
       //mmmmmmmmmmmmmmmmmmmmmmmmm
       if( sscanf(buff1,"%d, %lf\n", &i_n, &d_dum1)!=2 ){
       // Assume string = element title and radius
       // Skip since info already filled in structure
       } else {
       // Assume string = multipole n, field (or field gradient)
         if(do_print) printf("i_CHI %d multipole n %d, fld %lf\n",
           i_CHI, i_n, d_dum1 );
         fld_arr[i_Nmult][i_CHI] = d_dum1;
         i_Nmult++;
       }
       //mmmmmmmmmmmmmmmmmmmmmmmmm END
         }
       }
       fclose(fp1);
    //nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn END
*/
//    }
  } // i_CHI sssssssssssssssssssss END  
  return;
} //read_multi_COSY_OUTPUT_files

void write_arrays_to_file_V1( char *ffname )
{
  //pppppppppppppppppppppppppppppppppppppppppppppp
  // print all multipole fields to file
  printf(" ---- write_arrays_to_file_V1:\n");
  char str[1064];
  FILE *fp2;
  sprintf(str,"cat %s > %s", fheaders, ffname);
  //cout<<str<<endl;
  system(str); 
  //cout<<str<<endl; printf("TEST .\n"); return;
  fp2 = fopen( ffname, "a");
  fprintf(fp2,"#-----\n"); // this filler line is needed for \n
  fprintf(fp2,"FORMAT_V1\n"); //first line of data that denotes format
  fprintf(fp2,"name,R[m],n_order");
  for( i_CHI=0; i_CHI<Nfld; i_CHI++) fprintf(fp2,",%lf",CHIM[i_CHI]);
  fprintf(fp2,"\n");
  for( i_Nmult=0; i_Nmult<Nmult; i_Nmult++){
    fprintf(fp2,"%s,%lf,%d", 
      name_arr[i_Nmult], 
      R_arr[i_Nmult],
      n_arr[i_Nmult] );
    for( i_CHI=0; i_CHI<Nfld; i_CHI++){
      fprintf(fp2,",%lf", fld_arr[i_Nmult][i_CHI] );
    }
    fprintf(fp2,"\n");
  }
  fprintf(fp2,"name,R[m],n_order");
  for( i_CHI=0; i_CHI<Nfld; i_CHI++) fprintf(fp2,",%lf",CHIM2[i_CHI]);
  fprintf(fp2,"\n");
  fclose(fp2);
  printf("... write_arrays_to_file_V1: %s\n", ffname);
  //pppppppppppppppppppppppppppppppppppppppppppppp END
  return;
}

void read_csv_to_arrays_V1( char *ffname )
{  
  printf(" ---- read_csv_to_arrays_V1:\n");
  FILE *fp1;
  char buff1[LLEN];
  char *pch;
  char s_name1[LLEN]="elem_base_name";
  char s_name2[16]="n_multipole", s_dum1[64]="dummy", s_dum2[64];
  int i_n;
  double d_dum1, d_R;
  bool do_print=1;  
  i_CHI = 0;
  char com[2]="#";
  printf("  reading file: %s\n", ffname);
  fp1 = fopen( ffname, "r");
  if(fp1==NULL){ printf(" FILE error. %s\n", ffname); exit(0); }
  //read comment lines
  sprintf(buff1,"#");
  while( strncmp(buff1,com,1)==0 ) fgets( buff1, LLEN, fp1);
  if( strncmp(buff1,"FORMAT_V1",9)!=0 ){
    printf(" ERROR: Expecting format FORMAT_V1, but instead read %s\n",buff1);
    printf("        Aborting.\n");
  }
    fgets( buff1, LLEN, fp1);
    cout<<" Titles: "<<buff1; //exit(0);
    pch=NULL;
    pch = strtok( buff1,",");//name
    pch = strtok( NULL,",");//R
    pch = strtok( NULL,",");//n_order
    // CHI values begin
    pch = strtok( NULL,",");
    while(pch!=NULL){
      sscanf(pch, "%lf", &CHIM[i_CHI] );
      i_CHI++;
      if(pch==NULL) continue;
      pch = strtok( NULL,",");
    }
    Nfld = i_CHI;
  printf(" Total field values read from CHI line, Nfld = %d\n", Nfld);
  //for(i_CHI=0; i_CHI<Nfld; i_CHI++) printf("%d %lf\n",i_CHI,CHIM[i_CHI]);
  //
  // Now read each multipole and its fields
  i_Nmult = 0;
  while( !feof(fp1) ){
   if( fgets( buff1, LLEN, fp1)!=NULL && strncmp(buff1,com,1)!=0 ) {
     //--------
     //cout<<buff1;
     pch=NULL;
     pch = strtok( buff1,",");//name
       sscanf(pch,"%s", name_arr[i_Nmult] );
     pch = strtok( NULL,",");//R
       sscanf(pch,"%lf", &R_arr[i_Nmult] );
     pch = strtok( NULL,",");//n_order
       sscanf(pch,"%d", &n_arr[i_Nmult] );
     // fld values begin
     for(i_CHI=0; i_CHI<Nfld; i_CHI++){
       pch = strtok( NULL,",");
       if(pch==NULL){ 
         printf(" Error in field value. i_Nmult=%d i_CHI=%d\n",i_Nmult,i_CHI);
         exit(-2);
       }
       sscanf(pch,"%lf", &fld_arr[i_Nmult][i_CHI] );
     }
     //--------
     i_Nmult++;
   }
  }
  Nmult = i_Nmult;
  printf(" Total multipoles read from file, Nmult = %d\n", Nmult);
  fclose(fp1);
  return;
}

void read_n_apply_translation_key( char *ffname )
{  
  printf(" ---- read_n_apply_translation_key:\n");
  FILE *fp1;
  char buff1[LLEN];
  char *pch;
  fp1 = fopen( ffname, "r");
  if(fp1==NULL){ printf(" FILE error. %s\n", ffname); exit(0); }
  //read titles lines
  fgets( buff1, LLEN, fp1);
  Ntr = 0;
  while( !feof(fp1) ){
   if( fgets( buff1, LLEN, fp1)!=NULL ) {
     //--------
     if( strlen(buff1)>5 && strncmp(buff1,",",1)!=0 ){
       //debug: cout<<Ntr<<": "<<buff1;
       pch=NULL;
       pch = strtok( buff1,",");//type or name on COSY lattice
       sscanf(pch,"%s", tr_type[Ntr] );
       pch = strtok( NULL,",");//n_order
       sscanf(pch,"%d", &tr_n_arr[Ntr] );
       pch = strtok( NULL,",");//section name
       sscanf(pch,"%s", tr_sec[Ntr] );
       pch = strtok( NULL,",");//name1
       sscanf(pch,"%s", tr_name1[Ntr] );
       pch = strtok( NULL,",");//name2
       sscanf(pch,"%s", tr_name2[Ntr] );
       sprintf(tr_name3[Ntr],"%s:%s",tr_sec[Ntr],tr_name1[Ntr]);
       
       if(debug1) printf("test\t%s;\t%d;\t%s;\t%s \n", 
         tr_type[Ntr], tr_n_arr[Ntr], tr_name1[Ntr], tr_name3[Ntr] );
       Ntr++;
     }
   }
  }
  fclose(fp1);
  // Apply tranlation
  if(trans_names) 
  {
   printf(" Total tranlation names read, Ntr = %d\n", Ntr);
   for(int i=0; i<Nmult; i++){
    for(int j=0; j<Ntr; j++){ 
      //&& n_arr[i]==tr_n_arr[j]
      //  strcmp(name_arr[i],tr_type[j])==0
      //printf(" i%d j%d, n %d, name1 %s, type %s\n", i, j, n_arr[i], tr_name1[j], tr_type[j] );
      if( strcmp(name_arr[i],tr_type[j])==0 && n_arr[i]==tr_n_arr[j] ){
        //name_arr[i]=tr_name1[j];
        if (out_name_opt==1) {
          sprintf(name_arr[i], "%s", tr_name1[j]);
        } else if (out_name_opt==2) {
          sprintf(name_arr[i], "%s", tr_name2[j]);
        } else if (out_name_opt==3) {
          sprintf(name_arr[i], "%s", tr_name3[j]);
        } else { 
          printf("    ABORT!!!: INVALID out_name_opt = %i \n",out_name_opt);
          exit(0); 
        }
        //
        if(debug1) cout<<name_arr[i]<<"  "<<tr_n_arr[j]<<"  "<<tr_name1[j]<<endl;
      }
    }
   }
   printf("...read_n_apply_translation_key\n");
  }
  return;
}


void mpole_data_PROCESS_PS( int opt1=2, double CHIM_sing = 4.5 )
{
  /* opt1 =1, single file at CHIM_sing
          =0, multiple files
  */      

  //Develop a set of algorithms to read and store multipole field
  //  (or field gradient) data for use in various applications 
  //  for controls and optics simulations.

  //======== Use to append field values from separate files ===========
  //-Generare list of files names. Assumes known format, range and step size
  //  Known form of file names: GRAD_CHIM_?.dat, ?=magnetic rigidity (T-m)
  //  This method ensures ascending order without need to sort names.

  // change directory to where data exists
  gSystem->ChangeDirectory(dir_data);

  // directory with GRAD .dat file(s) from COSY output
  char subdir[] = "";
  char fOUT[500] = "collected_output_GRAD_PS.csv" ; //output file

  if (opt1==1) {
    //OPTION-1: For case of single file, set the name of file
    Nfld = 1 ;
    char fname[256];
    //---gradients
    sprintf( fname, "CHIM_%06.4f_GRAD.dat", CHIM_sing);
    sprintf( fname_arr[0], "%s%s", subdir, fname );  //input file
    //sprintf( fOUT, "collected_output_GRAD.csv" );  //output file
    read_multi_COSY_OUTPUT_files(); //can be single file
    read_n_apply_translation_key(ftrans); //names used for tranlation
    write_arrays_to_file_V1(fOUT);
    //---Amps
    sprintf( fname, "CHIM_%06.4f_AMPS.dat", CHIM_sing);
    sprintf( fname_arr[0], "%s%s", subdir, fname );  //input file
    sprintf( fOUT, "collected_output_AMPS.csv" );  //output file
    read_multi_COSY_OUTPUT_files(); //can be single file 
    read_n_apply_translation_key(ftrans); //names used for tranlation
    write_arrays_to_file_V1(fOUT);
    //
    
  } else if (opt1==2) {
    Nfld = 1 ;
    char fname[256];
    //---gradients
    sprintf( fname, "PS_NEW_GRAD.dat");
    sprintf( fname_arr[0], "%s%s", subdir, fname );  //input file
    //sprintf( fOUT, "collected_output_GRAD.csv" );  //output file
    read_multi_COSY_OUTPUT_files(); //can be single file
    read_n_apply_translation_key(ftrans); //names used for tranlation
    write_arrays_to_file_V1(fOUT);
    //---Amps
    sprintf( fname, "PS_NEW_AMPS.dat");
    sprintf( fname_arr[0], "%s%s", subdir, fname );  //input file
    sprintf( fOUT, "collected_output_AMPS.csv" );  //output file
    read_multi_COSY_OUTPUT_files(); //can be single file 
    read_n_apply_translation_key(ftrans); //names used for tranlation
    write_arrays_to_file_V1(fOUT);
    //

  } else if (opt1==0) {
    // OPTION-2: For case of multiple files with common name scheme
    //sprintf( fOUT, "collected_output_GRAD.csv" );  //output file
    CHIM_0 = 0.2500 ; CHIM_F = 6.5000 ; d_CHI = 0.2500 ;
    generate_fname_arr_GRAD_CHIM_by_string( subdir );
    read_multi_COSY_OUTPUT_files(); //can be single file
    read_n_apply_translation_key(ftrans); //names used for tranlation
    write_arrays_to_file_V1(fOUT);
    // ...AMPS... option need implementation
    //
  } else {
    printf("  ERROR: INVALID opt=%i \n", opt1);
  }


  return;
  //Optional: convert from gradient to field at ref. radius (e.g. pole-tip)
  read_csv_to_arrays_V1( fOUT );

  //Convert to fields at R and write to another file
  for( i_Nmult=0; i_Nmult<Nmult; i_Nmult++){
    for( i_CHI=0; i_CHI<Nfld; i_CHI++){
      fld_arr[i_Nmult][i_CHI] *= pow( R_arr[i_Nmult], n_arr[i_Nmult]-1 ); 
    }
  }
  char fFLDR[] = "collected_output_FLDR.csv" ;
  write_arrays_to_file_V1( fFLDR );

  printf("end.\n") ;
//printf("test1\n");exit(0);
  return;
}
//mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm END
