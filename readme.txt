Recommended steps:

* Move this directory to another space for computation outside of the repository

* Make sure minimum files are in the directory:
 readme.txt
 my.fox
 clean.sh
 run_prep.sh
 task_gen.py
 ...

* Execute run_prep.sh to prepare links and copies of needed files from elsewhere

* task_gen.py - script to use python3 to run code and generate/process results

* Edit .fox and .py files to set file names and tasks to change/add/remove

* clean.sh - optional script to clean.sh script to delete past or files not needed.
  To protect from deleting permanently needed result files, it best to 
  keep such files in another directory (or subdirectory) 


Instruction for starting scripts for COSY calculation at current ‘Set’ values in Amps. 
(Requires at least 2 terminals for processing)
cd /files/shared/ap/HLA/aris/cosy_202212/
On terminal 1 (cosy script):
	cosy xxxxxxxx_Psv15_cycle.fox
	Select task (e.g.  1.01 Continuous Presep[targ-DB1] computation )
On terminal 2 (root scripts for output plots):
	root -l show_plots.c
Optional: Using firefox browser, open the output images of plots:
	cPlotBmEnv.gif -- 1st order beam envelope from COSY map computation
	cScatPlot.png -- tracked rays from COSY calculation


References:
source files-- 
  \\intranet\files\departments\AcceleratorPhysics\ARIS\Optics\aris-dev-portillo\optics_files\PSv15_k3_PAC1_v3.4.9\
  /departments/AcceleratorPhysics/ARIS/Optics/aris-dev-portillo/optics_files/PSv15_k3_PAC1_v3.4.9/