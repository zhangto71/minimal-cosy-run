{
/* ROOT6 compatible script
   - Convert .ps -> .pdf -> png
   - Arrange multiple pics in TCanvas
pdftoppm <pdf-file-convert> <output-file-name> -png 
 [x,y res.]          -rx <dpi-horizontal> -ry <dpi-vertical>
 [crop to left corner]          -x <hor pnt> -y <vert pnt>
 [width of crop area]           -W <width>  -H <height>
 [https://linux.die.net/man/1/pdftoppm]
%s.pdf %s
*/

  TStyle *myplain = new TStyle("myplain","myplain");
    myplain->SetCanvasColor(kGray);
    myplain->SetCanvasBorderMode(1);
    myplain->SetPadBorderMode(0);
    myplain->SetPadBorderSize(0);
    myplain->SetPadColor(0);
    myplain->SetPadLeftMargin(0.001);
    myplain->SetPadRightMargin(0.001);
    myplain->SetPadTopMargin(0.001);
    myplain->SetPadBottomMargin(0.001);
    myplain->SetTitleColor(0);
    myplain->SetStatColor(0);
  gROOT->SetStyle("myplain");

//char names[][20]={ "pic001", "pic002", "pic003", "pic004", "pic005", "pic006"};
  char names[][20]={ "pic005", "pic006", "pic003", "pic004", "pic007", "pic008"};
  char cmd[200];
  //
  for (int i=0; i<6; i++){
    //cout<<names[1]<<endl;
    sprintf(cmd,"ps2pdf %s.ps",names[i]);
    cout << cmd <<endl;
    system(cmd);
    //
    sprintf(cmd,"pdftoppm %s.pdf %s -png -rx 200 -ry 200 -x 120 -W 1900 -y 210 -H 1300",names[i], names[i]);
    cout << cmd <<endl;
    system(cmd);
    //
    sprintf(cmd,"rm %s.pdf",names[i]);
    cout << cmd <<endl;
    system(cmd);
  }
//
int cH=720;
TCanvas *canv6ps = new TCanvas("canv6ps", "canv6ps", 30,30, cH,cH*1.083);
canv6ps->Divide(2,3);
double xw, yw;
int k=0;
sprintf(cmd,"%s-1.png",names[k]);  TImage *i1 = TImage::Open(cmd); k++;
xw = i1->GetWidth(), yw = i1->GetHeight(); cout<<xw<<" "<<yw<<endl;
//i1->Zoom(0.05*xw, 0.1*yw, 0.8*xw, 0.8*yw);
sprintf(cmd,"%s-1.png",names[k]);  TImage *i2 = TImage::Open(cmd); k++;
xw = i2->GetWidth(), yw = i2->GetHeight(); cout<<xw<<" "<<yw<<endl;
//i2->Zoom(0.05*xw, 0.1*yw, 0.8*xw, 0.8*yw);
sprintf(cmd,"%s-1.png",names[k]);  TImage *i3 = TImage::Open(cmd); k++;
sprintf(cmd,"%s-1.png",names[k]);  TImage *i4 = TImage::Open(cmd); k++;
sprintf(cmd,"%s-1.png",names[k]);  TImage *i5 = TImage::Open(cmd); k++;
sprintf(cmd,"%s-1.png",names[k]);  TImage *i6 = TImage::Open(cmd); k++;
TPad *p1 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(1); i1->Draw();
TPad *p2 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(2); i2->Draw();
TPad *p3 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(3); i3->Draw();
TPad *p4 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(4); i4->Draw();
TPad *p5 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(5); i5->Draw();
TPad *p6 = new TPad("i", "i", 0,0, 1,1); canv6ps->cd(6); i6->Draw();

canv6ps->SaveAs("canv6ps.png");
system("rm pic*-1.png");
return;
}

