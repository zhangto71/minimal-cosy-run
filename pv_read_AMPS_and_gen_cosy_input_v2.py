#!/usr/bin/python3
# Generate COSY input file from PV values
import os, sys
from epics import PV, caget, caput, cainfo

print(" ---- pv_read_AMPS_and_gen_cosy_input_v2.py:")
#test_epics = cainfo('FS_F1S2:PSQ_D1441:I_CSET')
#print("test_epics= ")
#print(test_epics)
#exit()

write_cosy_input_PS=True
if (write_cosy_input_PS):
  #print(" ---- write_cosy_input_PS :")
  set_type=":I_CSET"
  pvn="-"
  fp = open("1_updated.AMPS", "w")
  
  #Default values
  BrhoF1S1 = '4.1'
  BrhoF1S2 = '4.2'
  #Read PV values for Brho
  BrhoF1S1 =  caget("RBT_BTS01:BEAM:BRHO_BOOK")
  BrhoF1S2 =  caget("RBT_BTS02:BEAM:BRHO_BOOK") 
  # define reference particle RRP
  RPR_M = 106; RPR_Z = 50; RPR_Q = 50; # reference particle throughout
  # define wedge
  PSw_M=26.982; PSw_Z=13; PSw_den=2.66;    #PS wedge material
  PSw_a0=0.002195; PSw_a1=-0.003945; PSw_a2=0; PSw_a3=0; #wedge thick & shape
  
  fp.write("RW_LATTICE_PS_V15\n")
  fp.write("M_INPUT_TYPE\n")
  fp.write("2\n")
  fp.write("# FF_DEFAULTS\n")
  fp.write("1\n")
  fp.write("1\n")
  fp.write("0\n")
  fp.write("2.5\n")
  fp.write("0\n")
  fp.write("0\n")
  fp.write("0\n")
  fp.write("BrhoMin\n")
  fp.write(".25\n")
  fp.write("BrhoMax\n")
  fp.write("8\n")
  fp.write("SB_params, PX PA R12 PY PB R34 PT PD R56 PG PZ\n")
  fp.write("0.3E-3\n") #xm
  fp.write("2.3E-3\n")
  fp.write("0\n")
  fp.write("0.3E-3\n") #ym
  fp.write("2.3E-3\n")
  fp.write("0\n")
  fp.write("0.163\n")   #dl
  fp.write("1.0E-3\n")  #dE/E
  fp.write("0\n")
  fp.write("0\n")       #dM
  fp.write("0\n")       #dZ
  
  fp.write("# RPR_\n")      #1
  fp.write(str(BrhoF1S1)+"\n")     #T-m
  fp.write(str(RPR_M)+"\n")    #M, AMU
  fp.write(str(RPR_Z)+"\n")          #Z
  fp.write(str(RPR_Q)+"\n")          #Q
  
  fp.write("WIQ1\n")
  pvn="FS_F1S1:PSQ_D1013"+set_type; fp.write(str(caget(pvn))+"\n")

  fp.write("WIQ2\n")
  pvn="FS_F1S1:PSQ_D1024"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSS_D1024"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSO_D1024"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("WIQ3\n")
  pvn="FS_F1S1:PSQ_D1035"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSS_D1035"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSO_D1035"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("FSD1_SCD1\n")
  pvn="FS_F1S1:PSD_D1064"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("FSD1_SCD2\n")
  pvn="FS_F1S1:PSD_D1108"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("WIQ4\n")
  pvn="FS_F1S1:PSQ_D1137"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSS_D1137"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSO_D1137"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("WIQ5\n")
  pvn="FS_F1S1:PSQ_D1148"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSS_D1148"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSO_D1148"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("WIQ7\n")
  pvn="FS_F1S1:PSQ_D1170"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSS_D1170"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S1:PSO_D1170"+set_type; fp.write(str(caget(pvn))+"\n")

  fp.write("# WEDGE\n")
  fp.write(str(PSw_M)+"\n")     # Avg Mass
  fp.write(str(PSw_Z)+"\n")         # Z
  fp.write(str(PSw_den)+"\n")      # g/cm3
  fp.write(str()+"0.2\n")        # half-gap
  fp.write(str(PSw_a0)+"0\n")        # thick [m]
  fp.write(str(PSw_a1)+"0\n")        # ang   [rad]
  fp.write(str(PSw_a2)+"0\n")        # ord2  [1/m]
  fp.write(str(PSw_a3)+"0\n")        # ord3  [1/m^2]

  fp.write("# RPR_\n")      #2
  fp.write(str(BrhoF1S2)+"\n")     #T-m
  fp.write(str(RPR_M)+"\n")    #M, AMU
  fp.write(str(RPR_Z)+"\n")          #Z
  fp.write(str(RPR_Q)+"\n")          #Q
  
  fp.write("CIQT1A\n")
  pvn="FS_F1S2:PSQ_D1195"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1195"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1195"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT1B\n")
  pvn="FS_F1S2:PSQ_D1207"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1207"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1207"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT1C\n")
  pvn="FS_F1S2:PSQ_D1218"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1218"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1218"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("FSD2_SCD3\n")
  pvn="FS_F1S2:PSD_D1246"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT2A\n")
  pvn="FS_F1S2:PSQ_D1288"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1288"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1288"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT2B\n")
  pvn="FS_F1S2:PSQ_D1299"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1299"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1299"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT2C\n")
  pvn="FS_F1S2:PSQ_D1311"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1311"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1311"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT3A\n")
  pvn="FS_F1S2:PSQ_D1338"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1338"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1338"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT3B\n")
  pvn="FS_F1S2:PSQ_D1349"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1349"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1349"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT3C\n")
  pvn="FS_F1S2:PSQ_D1361"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1361"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1361"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("FSD2_SCD4\n")
  pvn="FS_F1S2:PSD_D1402"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT4A\n")
  pvn="FS_F1S2:PSQ_D1430"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1430"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1430"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT4B\n")
  pvn="FS_F1S2:PSQ_D1441"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1441"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1441"+set_type; fp.write(str(caget(pvn))+"\n")
  fp.write("CIQT4C\n")
  pvn="FS_F1S2:PSQ_D1453"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSS_D1453"+set_type; fp.write(str(caget(pvn))+"\n")
  pvn="FS_F1S2:PSO_D1453"+set_type; fp.write(str(caget(pvn))+"\n")
  #
  fp.write("END\n")
  fp.close()
  print("       ... pv read completed.")

exit()
