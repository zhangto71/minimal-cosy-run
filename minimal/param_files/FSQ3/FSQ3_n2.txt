#2014-08 MH
# /mnt/simulations/fribesmag/PreseparatorQuadrupoles/FSQ3/TOSCA/
#  Gradient1.9Tpm/fsq3_1.9Tpm_ref10_max14.input
#  Gradient10Tpm/fsq3_10Tpm_ref10_max16.input
# (after refit to warm bore radius of 13 cm) (MH)
M5_PARAM_V01
  0.13, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
2, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  9.9, field gradient limit
SET1 #1.9Tpm_n=2
 0,0,0,0, [Amps]
 0.250311,0,0,0, [Tesla]
 0.719907,0,0,0, Leff[m]
 0.180201,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
0.0525905,0,0,0
4.5171,0,0,0
-0.227906,0,0,0
0.514315,0,0,0
-0.422979,0,0,0
0.0678871,0,0,0
IEE2
0.0525905,0,0,0
4.5171,0,0,0
-0.227906,0,0,0
0.514315,0,0,0
-0.422979,0,0,0
0.0678871,0,0,0
SET2 #10Tpm_n=2
 0,0,0,0, [Amps]
 1.286712,0,0,0, [Tesla]
 0.689482,0,0,0, Leff[m]
 0.887164,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
-0.0200085,0,0,0
4.16696,0,0,0
0.380385,0,0,0
0.294419,0,0,0
-0.279779,0,0,0
0.0377134,0,0,0
IEE2
-0.0200085,0,0,0
4.16696,0,0,0
0.380385,0,0,0
0.294419,0,0,0
-0.279779,0,0,0
0.0377134,0,0,0
END
