#2014-12 MP--
# Adopted Enge parameters from FSQ2 octupole
# Adopted Leff value from Chouhan: 2014_12_16_sext_n_oct_params.pptx
M5_PARAM_V01
  0.13, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
4, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  21.4, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.029414,0,0,0, [Tesla]
 0.72,0,0,0, Leff[m]
 0.0212,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
0.0019757,0,0,0
9.06723,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
IEE2
0.0019757,0,0,0
9.06723,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
END
