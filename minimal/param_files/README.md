Collection of directories that have element parameter files used by COSY or other beam simulation codes. Most current files include values that are dependent on excitation current such as magnet field, gradient, effective length, effective bend radius, Enge coefficients, and maybe others.

Many subdirectories, such as FSQ1,FSQ2,... are earlier versions of FSQ1_2020,FSQ2_S2_2020,... and are used by previous versions of .fox, hence are kept.

* Noteable log of changes to files:

**2022-03-23:MP**
- Add HRS directory for keeping parameters files

**2022-03-11:MP**
- Add table form of param_files for FSQA thru FSQE, S8Q & THQS

**2022-02-02:MP**
- git add param_files/THQS/THQS_n2.txt  (add current 0. This still needs update
   with ...table.csv)
- git add param_files/THQL/THQL_n2_table.csv  (newer version of params)
  git add param_files/THQS/THQL_n2_table.csv

**2022-02-02:MP**
- Update param_files/... for preseparator magnets. Mainly to include zero values for splines and table for THQL.
