#2015-06 MP
# /mnt/simulations/fribesmag/PreseparatorQuadrupoles/
#  FSQ5/TOSCA/Gradient1.1Tpm/fsq5_1.1Tpm_ref16_max18.input
#  FSQ5/TOSCA/Gradient9.4Tpm/fsq5_9.4Tpm_ref16_max20.input
# Fit to n=6 component induced from n=2
M5_PARAM_V01
  0.2, Reference radius to use with coefficients.
  0., yoke length (dummy value for now)
6, IMP order of multipole
2, IMP_REF multipole for effective length of M5
 0., field gradient limit (should be max allowed for set fields)
SET1  # 1.1Tpm at n=2
 0,0,0,0, [Amps]
 0.003728,0,0,0, [Tesla]
 0.820888,0,0,0, Leff[m]
 0.003060,0,0,0, FldInteg [T-m]
 0, stdev of residuals
2, EXTF
IEE1
0
5
0
0
0
0
1.93024
-0.64254
0.25
-1.0871
-0.8582
0.4
IEE2
0
5
0
0
0
0
1.93024
-0.64254
0.25
-1.0871
-0.8582
0.4
SET2  # 9.4Tpm at n=2
 0,0,0,0, [Amps]
 0.009677,0,0,0, [Tesla]
 0.782112,0,0,0, Leff[m]
 0.007564,0,0,0, FldInteg [T-m]
 0, stdev of residuals
0, EXTF
IEE1
0
5
0
0
0
0
IEE2
0
5
0
0
0
0
END
