#2014-12
# Adopted Enge parameters from FSQ7 
# Adopted Leff value from Chouhan: 2014_12_16_sext_n_oct_params.pptx
M5_PARAM_V01
  0.20, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
4, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  14.95, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.119633,0,0,0, [Tesla]
 0.8086700,0,0,0, Leff[m]
 0.0967,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
0.0419469,0,0,0
9.17005,0,0,0
0.262031,0,0,0
1.95682,0,0,0
-8.18597,0,0,0
41.7681,0,0,0
IEE2
0.0419469,0,0,0
9.17005,0,0,0
0.262031,0,0,0
1.95682,0,0,0
-8.18597,0,0,0
41.7681,0,0,0
END
