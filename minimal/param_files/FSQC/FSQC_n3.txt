#AC227 n=3
# comments:
# Parameters for read/write 
# Magnet: AC227SX 2012-Nov, Type B (of A1900) quadrupole from "T2-spare triplet"
# Enge coefficieints for IMP=3 (SEXTupole), fit by minimizing difference       
#  between COSY POTFLD and B-radial component measured versus z in FR 4 mode.
#  RSS is the residual sum squared over all the data points in the fit.
#  Field map data acquired at N4 vault in Dec 2011                              
#_________________________________________________________________________  
M5_PARAM_V01
  0.150000, Reference radius to use with coefficients.
  0.325000, yoke length
3, IMP order of multipole
2, IMP_REF multipole for eff length of M5
  6.8, field gradient limit
SET1
36,0.15106E+03,0,0, [Amps]
  0.238300,0,0,0, [Tesla]
  0.416780,0,0,0, Leff[m]
0,0,0,0, T-m
  0.020959, RSS of COSY model to data
0, EXTF
IEE1
 -0.031987,0,0,0
  9.106800,0,0,0
 -0.206730,0,0,0
  1.927300,0,0,0
 -0.291640,0,0,0
0,0,0,0
IEE2
  0.055383,0,0,0
  9.106800,0,0,0
 -0.206730,0,0,0
  1.927300,0,0,0
 -0.291640,0,0,0
0,0,0,0
END
