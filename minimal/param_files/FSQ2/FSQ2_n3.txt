#2014-12 MP--
# Fit to FSQ2 sextupole TOSCA data
# .../FSQ2_sextupole/TOSCA_90degrees/2014-12_Grad4.4/fsq2_sext.input
# Fourier analyzed using ReanalyzeQuad_v06_dev.C using
# Leff value from Chouhan: 2014_12_16_sext_n_oct_params.pptx
M5_PARAM_V01
  0.13, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
3, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  4.39, field gradient limit
SET_1
 0,0,0,0, [Amps]
 0.074112,0,0,0, [Tesla]
 0.81,0,0,0, Leff[m]
 0.059954,0,0,0, [T-m]
 0.0297, Standard deviation 
 0, EXTF
IEE1
-0.00918896,0,0,0
7.23668,0,0,0
0.599035,0,0,0
2.57306,0,0,0
0,0,0,0
0,0,0,0
IEE2
-0.00918896,0,0,0
7.23668,0,0,0
0.599035,0,0,0
2.57306,0,0,0
0,0,0,0
0,0,0,0
END
