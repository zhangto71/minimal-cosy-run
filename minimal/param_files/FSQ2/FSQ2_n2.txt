#2014-08 MH--
# /mnt/simulations/fribesmag/PreseparatorQuadrupoles/
# FSQ2/TOSCA/Gradient13.1Tpm_2014-06_analysis/fsq2_13.1Tpm_ref10_max16.input
# (after refit to warm bore radius of 13 cm) (MH)
M5_PARAM_V01
  0.13, Reference radius to use with coefficients.
  0.00000, yoke length (dummy value for now)
2, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  13.09, field gradient limit
SET1
 0,0,0,0, [Amps]
 1.70125,0,0,0, [Tesla]
 0.783818,0,0,0, Leff[m]
 1.33347,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
-0.0885658,0,0,0
4.17181,0,0,0
0.583444,0,0,0
0.291737,0,0,0
0.022574,0,0,0
0.143634,0,0,0
IEE2
-0.0885658,0,0,0
4.17181,0,0,0
0.583444,0,0,0
0.291737,0,0,0
0.022574,0,0,0
0.143634,0,0,0
END
