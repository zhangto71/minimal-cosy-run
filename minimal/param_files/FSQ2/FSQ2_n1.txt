#2014-12 MP--
# Fit to FSQ2 dipole component of TOSCA data
# .../FSQ2_sextupole/TOSCA_90degrees/2014-12_Grad4.4/fsq2_sext.input
# Fourier analyzed using ReanalyzeQuad_v06_dev.C using
# Leff value from Chouhan: 2014_12_16_sext_n_oct_params.pptx
# fsq2_induced_dipole.input
M5_PARAM_V01
  0.13, Reference radius to use with coefficients.
  0., yoke length (dummy for now)
1, IMP order of multipole
2, IMP_REF multipole for effective length of M5
0, field gradient limit
SET_
 0,0,0,0, [Amps]
 0.005066,0,0,0, [Tesla]
 0.81,0,0,0, Leff[m]
 0.004004,0,0,0, [T-m]
 0.0, Standard deviation 
 0, EXTF
IEE1
0.186594,0,0,0
3.38007,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
IEE2
0.186594,0,0,0
3.38007,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
END
