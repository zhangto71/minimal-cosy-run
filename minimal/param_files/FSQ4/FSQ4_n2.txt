#2014-08 MH
# /mnt/simulations/fribesmag/PreseparatorQuadrupoles/FSQ4/TOSCA
#  Gradient2.5Tpm/fsq4_2.5Tpm_ref18_max20.input
#  Gradient8.5Tpm/fsq4_8.5Tpm_ref18_max20.input
# (after refit to warm bore radius of 22 cm) (MH)
M5_PARAM_V01
  0.220000, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
2, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  8.45, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.559315,0,0,0, [Tesla]
 0.774811,0,0,0, Leff[m]
 0.433364,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
0.085976,0,0,0
5.48378,0,0,0
-0.360939,0,0,0
0.817294,0,0,0
-0.994559,0,0,0
0.21863,0,0,0
IEE2
0.085976,0,0,0
5.48378,0,0,0
-0.360939,0,0,0
0.817294,0,0,0
-0.994559,0,0,0
0.21863,0,0,0
SET2
 0,0,0,0, [Amps]
 1.860176,0,0,0, [Tesla]
 0.751243,0,0,0, Leff[m]
 1.397445,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
0.0413207,0,0,0
5.30898,0,0,0
0.40589,0,0,0
0.629163,0,0,0
-1.32417,0,0,0
0.801716,0,0,0
IEE2
0.0413207,0,0,0
5.30898,0,0,0
0.40589,0,0,0
0.629163,0,0,0
-1.32417,0,0,0
0.801716,0,0,0
END
