#2014-08 MH
# /mnt/simulations/fribesmag/PreseparatorQuadrupoles/
#  FSQ4_Sextupole/TOSCA/Gradient4.8Tpm2/fsq4_4.8Tpm2_ref18_max18.input
# (after refit to warm bore radius of 22 cm) (MH)
M5_PARAM_V01
  0.220000, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
3, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  5.29, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.256103,0,0,0, [Tesla]
 0.720923,0,0,0, Leff[m]
 0.184631,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
-0.0146424,0,0,0
7.62055,0,0,0
0.353057,0,0,0
2.12956,0,0,0
0.277474,0,0,0
4.20227,0,0,0
IEE2
-0.0146424,0,0,0
7.62055,0,0,0
0.353057,0,0,0
2.12956,0,0,0
0.277474,0,0,0
4.20227,0,0,0
END
