#2014-08 
# Adopted Enge parameters from FSQ4 
# Adopted Leff value from Chouhan: 2014_12_16_sext_n_oct_params.pptx
M5_PARAM_V01
  0.17, Reference radius to use with coefficients.
  0.00000, yoke length (dummy for now)
4, IMP order of multipole
2, IMP_REF multipole for effective length of M5
  17.2, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.0070617,0,0,0, [Tesla]
 0.7329510,0,0,0, Leff[m]
 0.0052,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
-0.0244241,0,0,0
9.06503,0,0,0
0.56328,0,0,0
4.23049,0,0,0
3.38109,0,0,0
9.55281,0,0,0
IEE2
-0.0244241,0,0,0
9.06503,0,0,0
0.56328,0,0,0
4.23049,0,0,0
3.38109,0,0,0
9.55281,0,0,0
SET2
 0,0,0,0, [Amps]
 0.0070617,0,0,0, [Tesla]
 0.7329510,0,0,0, Leff[m]
 0.0052,0,0,0, [T-m]
 0.0, RSS of fit
 0, EXTF
IEE1
-0.0244249,0,0,0
9.06505,0,0,0
0.563347,0,0,0
4.23093,0,0,0
3.38053,0,0,0
9.55041,0,0,0
IEE2
-0.0244249,0,0,0
9.06505,0,0,0
0.563347,0,0,0
4.23093,0,0,0
3.38053,0,0,0
9.55041,0,0,0
END
