# A1900 "D1" refit of all dipole data in 45 degree bend mode. Probe positions 
# not assigned yet but would likely have no impact since W is uniform in the 
# model. Source of analysis and results:
# /projects/a2400/testMOTER/A1900/A1900_info_from_DeKamp/A19dipole/Refit_A1900-45deg_2015-03/
#version tag:
MSS_V01
 3.100000, Design bend radius
 45.000000, Bend angle
 0.045000, Half-gap
 1.000000, Entrance edge angle
 0, Entrance curvature
 1.000000, Exit edge angle
 0, Exit curvature
 2, Number of probe positions
SET 1
 2, EXTF extended function flag
 2.033045, Tm, Brho=By*Rdipole optimum
 0.655821, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 40.000000, Amps
 0.003973, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.97211e-01 
 2.36436e+00 
-9.86320e-01 
 3.90920e-01 
 0.00000e+00 
 0.00000e+00 
-6.47200e-03 
-6.00000e-01 
 2.10000e+00 
-1.00000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.97211e-01 
 2.36436e+00 
-9.86320e-01 
 3.90920e-01 
 0.00000e+00 
 0.00000e+00 
-6.47200e-03 
-6.00000e-01 
 2.10000e+00 
-1.00000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.006166737609328
SET 2
 2, EXTF extended function flag
 3.034999, Tm, Brho=By*Rdipole optimum
 0.979032, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 60.000000, Amps
 0.005452, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.82621e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.82621e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.006349562255336
SET 3
 2, EXTF extended function flag
 3.766655, Tm, Brho=By*Rdipole optimum
 1.215050, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 75.000000, Amps
 0.006230, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.84184e-01 
 2.32110e+00 
-7.20688e-01 
 2.91625e-01 
 0.00000e+00 
 0.00000e+00 
-6.58750e-03 
-6.00000e-01 
 2.10000e+00 
 1.17750e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.84184e-01 
 2.32110e+00 
-7.20688e-01 
 2.91625e-01 
 0.00000e+00 
 0.00000e+00 
-6.58750e-03 
-6.00000e-01 
 2.10000e+00 
 1.17750e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.007390511527605
SET 4
 2, EXTF extended function flag
 5.293346, Tm, Brho=By*Rdipole optimum
 1.707531, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 120.000000, Amps
 0.006934, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.25648e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.25648e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.014685646325235
SET 5
 2, EXTF extended function flag
 5.625567, Tm, Brho=By*Rdipole optimum
 1.814699, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 135.000000, Amps
 0.006957, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.23408e-01 
 2.16372e+00 
-3.60507e-01 
 1.79545e-01 
 0.00000e+00 
 0.00000e+00 
-2.39995e-02 
-6.00000e-01 
 2.10000e+00 
 3.36750e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.23408e-01 
 2.16372e+00 
-3.60507e-01 
 1.79545e-01 
 0.00000e+00 
 0.00000e+00 
-2.39995e-02 
-6.00000e-01 
 2.10000e+00 
 3.36750e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.017782114418670
SET 6
 2, EXTF extended function flag
 6.211361, Tm, Brho=By*Rdipole optimum
 2.003665, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 170.000000, Amps
 0.005719, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.76188e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.76188e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.02594588640424
SET 7
 2, EXTF extended function flag
 6.337512, Tm, Brho=By*Rdipole optimum
 2.044359, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 180.000000, Amps
 0.004801, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.51639e-01 
 1.97670e+00 
-1.69280e-01 
 1.43680e-01 
 0.00000e+00 
 0.00000e+00 
-5.13280e-02 
-6.00000e-01 
 2.10000e+00 
 5.01000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.51639e-01 
 1.97670e+00 
-1.69280e-01 
 1.43680e-01 
 0.00000e+00 
 0.00000e+00 
-5.13280e-02 
-6.00000e-01 
 2.10000e+00 
 5.01000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.028473872815622
SET 8
 2, EXTF extended function flag
 6.457706, Tm, Brho=By*Rdipole optimum
 2.083131, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 190.000000, Amps
 0.006171, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.17589e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.17589e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.030908509463329
END