#Output from ROOT fit script messed with by hand
# this is for A116DS bending to the right (all lines but B-line)
#version tag:
MSS_V01
 3.105783, Bend radius from "Current(2014)TransferHallOpticsFromTransport.xlsx"
 22.650000, Bend angle from "Current(2014)TransferHallOpticsFromTransport.xlsx"
 0.035000, Half-gap
 11.32, Entrance edge angle
 0, Entrance curvature
 11.32, Exit edge angle
 0, Exit curvature
 1, Number of probe positions
SET 1
 0, EXTF extended function flag
 1.5521, Tm, Brho=By*Rdipole
 0.499745, By at mid-bend position (NomField from TOSCA data)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
 0, Amps
 0.000, sqrtRSS for residual of data to ROOT_MSS model
IEE1_ENGEC
0.351873
2.24209
-0.791909
0.601577
-0.180205
0.0170671
IEE2_ENGEC
0.351873
2.24209
-0.791909
0.601577
-0.180205
0.0170671
W_COEFS: i,j,W(i,j)
1, 1,  1.000110220533883
1, 2,  0.00000e+00
1, 3,  0.00000e+00
1, 4,  0.00000e+00
1, 5,  0.00000e+00
1, 6,  0.00000e+00
1, 7,  0.00000e+00
2, 1,  0.00000e+00
2, 2,  0.00000e+00
2, 3,  0.00000e+00
2, 4,  0.00000e+00
2, 5,  0.00000e+00
2, 6,  0.00000e+00
2, 7,  0.00000e+00
3, 1,  0.00000e+00
3, 2,  0.00000e+00
3, 3,  0.00000e+00
3, 4,  0.00000e+00
3, 5,  0.00000e+00
3, 6,  0.00000e+00
3, 7,  0.00000e+00
4, 1,  0.00000e+00
4, 2,  0.00000e+00
4, 3,  0.00000e+00
4, 4,  0.00000e+00
4, 5,  0.00000e+00
4, 6,  0.00000e+00
4, 7,  0.00000e+00
5, 1,  0.00000e+00
5, 2,  0.00000e+00
5, 3,  0.00000e+00
5, 4,  0.00000e+00
5, 5,  0.00000e+00
5, 6,  0.00000e+00
5, 7,  0.00000e+00
6, 1,  0.00000e+00
6, 2,  0.00000e+00
6, 3,  0.00000e+00
6, 4,  0.00000e+00
6, 5,  0.00000e+00
6, 6,  0.00000e+00
6, 7,  0.00000e+00
7, 1,  0.00000e+00
7, 2,  0.00000e+00
7, 3,  0.00000e+00
7, 4,  0.00000e+00
7, 5,  0.00000e+00
7, 6,  0.00000e+00
7, 7,  0.00000e+00
SET 2
 0, EXTF extended function flag
 3.1033356, Tm, Brho=By*Rdipole 
 0.999212, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
 0, Amps
 0.00, sqrtRSS for residual of data to ROOT_MSS model
IEE1_ENGEC
0.32846
2.27064
-0.787223
0.4305
-0.0739063
0.00387434
IEE2_ENGEC
0.32846
2.27064
-0.787223
0.4305
-0.0739063
0.00387434
W_COEFS: i,j,W(i,j)
1, 1,  0.9997901420666314
1, 2,  0.00000e+00
1, 3,  0.00000e+00
1, 4,  0.00000e+00
1, 5,  0.00000e+00
1, 6,  0.00000e+00
1, 7,  0.00000e+00
2, 1,  0.00000e+00
2, 2,  0.00000e+00
2, 3,  0.00000e+00
2, 4,  0.00000e+00
2, 5,  0.00000e+00
2, 6,  0.00000e+00
2, 7,  0.00000e+00
3, 1,  0.00000e+00
3, 2,  0.00000e+00
3, 3,  0.00000e+00
3, 4,  0.00000e+00
3, 5,  0.00000e+00
3, 6,  0.00000e+00
3, 7,  0.00000e+00
4, 1,  0.00000e+00
4, 2,  0.00000e+00
4, 3,  0.00000e+00
4, 4,  0.00000e+00
4, 5,  0.00000e+00
4, 6,  0.00000e+00
4, 7,  0.00000e+00
5, 1,  0.00000e+00
5, 2,  0.00000e+00
5, 3,  0.00000e+00
5, 4,  0.00000e+00
5, 5,  0.00000e+00
5, 6,  0.00000e+00
5, 7,  0.00000e+00
6, 1,  0.00000e+00
6, 2,  0.00000e+00
6, 3,  0.00000e+00
6, 4,  0.00000e+00
6, 5,  0.00000e+00
6, 6,  0.00000e+00
6, 7,  0.00000e+00
7, 1,  0.00000e+00
7, 2,  0.00000e+00
7, 3,  0.00000e+00
7, 4,  0.00000e+00
7, 5,  0.00000e+00
7, 6,  0.00000e+00
7, 7,  0.00000e+00
SET 3
 0, EXTF extended function flag
 4.55814, Tm, Brho=By*Rdipole 
 1.46763, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
 0, Amps
 0.00, sqrtRSS for residual of data to ROOT_MSS model
IEE1_ENGEC
0.165496
2.37296
-0.730117
0.0322583
0.172941
0.0262273
IEE2_ENGEC
0.165496
2.37296
-0.730117
0.0322583
0.172941
0.0262273
W_COEFS: i,j,W(i,j)
1, 1,  0.9980612952463942
1, 2,  0.00000e+00
1, 3,  0.00000e+00
1, 4,  0.00000e+00
1, 5,  0.00000e+00
1, 6,  0.00000e+00
1, 7,  0.00000e+00
2, 1,  0.00000e+00
2, 2,  0.00000e+00
2, 3,  0.00000e+00
2, 4,  0.00000e+00
2, 5,  0.00000e+00
2, 6,  0.00000e+00
2, 7,  0.00000e+00
3, 1,  0.00000e+00
3, 2,  0.00000e+00
3, 3,  0.00000e+00
3, 4,  0.00000e+00
3, 5,  0.00000e+00
3, 6,  0.00000e+00
3, 7,  0.00000e+00
4, 1,  0.00000e+00
4, 2,  0.00000e+00
4, 3,  0.00000e+00
4, 4,  0.00000e+00
4, 5,  0.00000e+00
4, 6,  0.00000e+00
4, 7,  0.00000e+00
5, 1,  0.00000e+00
5, 2,  0.00000e+00
5, 3,  0.00000e+00
5, 4,  0.00000e+00
5, 5,  0.00000e+00
5, 6,  0.00000e+00
5, 7,  0.00000e+00
6, 1,  0.00000e+00
6, 2,  0.00000e+00
6, 3,  0.00000e+00
6, 4,  0.00000e+00
6, 5,  0.00000e+00
6, 6,  0.00000e+00
6, 7,  0.00000e+00
7, 1,  0.00000e+00
7, 2,  0.00000e+00
7, 3,  0.00000e+00
7, 4,  0.00000e+00
7, 5,  0.00000e+00
7, 6,  0.00000e+00
7, 7,  0.00000e+00
END
#remainder is for highest calculated field, but fit of Enge+Takeda is giving trouble
SET 4
 1, EXTF extended function flag
 5.316299, Tm, Brho=By*Rdipole optimum
 1.71142, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
 0, Amps
 0.00, sqrtRSS for residual of data to ROOT_MSS model
IEE1_ENGEC
-0.108665
2.38266
-0.496487
-0.153042
0.178415
0.0456042
0.00856092
-20.625
98.6989
-4.26419
4.29936
IEE2_ENGEC
-0.108665
2.38266
-0.496487
-0.153042
0.178415
0.0456042
0.00856092
-20.625
98.6989
-4.26419
4.29936
W_COEFS: i,j,W(i,j)
1, 1,  0.9638744444383001      # this one may be wrong
1, 2,  0.00000e+00
1, 3,  0.00000e+00
1, 4,  0.00000e+00
1, 5,  0.00000e+00
1, 6,  0.00000e+00
1, 7,  0.00000e+00
2, 1,  0.00000e+00
2, 2,  0.00000e+00
2, 3,  0.00000e+00
2, 4,  0.00000e+00
2, 5,  0.00000e+00
2, 6,  0.00000e+00
2, 7,  0.00000e+00
3, 1,  0.00000e+00
3, 2,  0.00000e+00
3, 3,  0.00000e+00
3, 4,  0.00000e+00
3, 5,  0.00000e+00
3, 6,  0.00000e+00
3, 7,  0.00000e+00
4, 1,  0.00000e+00
4, 2,  0.00000e+00
4, 3,  0.00000e+00
4, 4,  0.00000e+00
4, 5,  0.00000e+00
4, 6,  0.00000e+00
4, 7,  0.00000e+00
5, 1,  0.00000e+00
5, 2,  0.00000e+00
5, 3,  0.00000e+00
5, 4,  0.00000e+00
5, 5,  0.00000e+00
5, 6,  0.00000e+00
5, 7,  0.00000e+00
6, 1,  0.00000e+00
6, 2,  0.00000e+00
6, 3,  0.00000e+00
6, 4,  0.00000e+00
6, 5,  0.00000e+00
6, 6,  0.00000e+00
6, 7,  0.00000e+00
7, 1,  0.00000e+00
7, 2,  0.00000e+00
7, 3,  0.00000e+00
7, 4,  0.00000e+00
7, 5,  0.00000e+00
7, 6,  0.00000e+00
7, 7,  0.00000e+00
END

