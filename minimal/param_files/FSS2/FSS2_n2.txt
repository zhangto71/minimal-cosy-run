#2015-02-09
# Parameters taken from results of the fit to TOSCA data. Values are copied 
#   from file, EngeGaussFitForCOSY_0.00T_B_n2.txt
#   which was generated using ReanalyzeQuad_v06_devG_test.C
# This quadrupole is induced by the octupole excitation and its field 
#  excitation dependence on n=4 is to be hard coded in the beam physics code.
M5_PARAM_V01
  0.200000, Reference radius to use with coefficients.
  0.4, yoke length (dummy for now)
2, IMP order of multipole
3, IMP_REF multipole for effective length of M5
  10, field gradient limit (no limit needed, hence set high)
SET1
 0,0,0,0, [Amps]
 0.002644,0,0,0, [Tesla]
 0.530781,0,0,0, Leff[m]
 0.001415,0,0,0, [T-m]
 0.0, RSS of fit
 2, EXTF
IEE1
-0.522904,0,0,0
21.9932,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0.686262,0,0,0
-0.212626,0,0,0
0.172197,0,0,0
-1.12486,0,0,0
0.167872,0,0,0
0.35,0,0,0
IEE2
-0.522904,0,0,0
21.9932,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0,0,0,0
0.686262,0,0,0
-0.212626,0,0,0
0.172197,0,0,0
-1.12486,0,0,0
0.167872,0,0,0
0.35,0,0,0
END

