#2015-02-09
# Parameters taken from results of the fit to TOSCA data. Values are copied 
#   from file, EngeGaussFitForCOSY_0.06T_B_n4.txt
#   which was generated using ReanalyzeQuad_v06_dev2.C
M5_PARAM_V01
  0.200000, Reference radius to use with coefficients.
  0.4, yoke length (dummy for now)
4, IMP order of multipole
3, IMP_REF multipole for effective length of M5
  7.7, field gradient limit
SET1
 0,0,0,0, [Amps]
 0.061740,0,0,0, [Tesla]
 0.530781,0,0,0, Leff[m]
 0.031234,0,0,0, [T-m]
 0.0, Standard deviation 
 0, EXTF
IEE1
0.294469,0,0,0
8.73783,0,0,0
0.20539,0,0,0
3.75925,0,0,0
-3.21178,0,0,0
0.254078,0,0,0
IEE2
0.294469,0,0,0
8.73783,0,0,0
0.20539,0,0,0
3.75925,0,0,0
-3.21178,0,0,0
0.254078,0,0,0
END
