# A1900 "D1" refit of all dipole data in 60 degree bend mode. Probe positions 
# not assigned yet but would likely have no impact since W is uniform in the 
# model. Source of analysis and results:
# /projects/a2400/testMOTER/A1900/A1900_info_from_DeKamp/A19dipole/Refit_FSDA-30deg_2015-03
#version tag:
MSS_V01
 4.576369, Design bend radius
 30.000000, Bend angle
 0.045000, Half-gap
 -6.500000, Entrance edge angle
 0, Entrance curvature
 -6.500000, Exit edge angle
 0, Exit curvature
 2, Number of probe positions
SET 1
 2, EXTF extended function flag
 3.002079, Tm, Brho=By*Rdipole optimum
 0.655996, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 40.000000, Amps
 0.004162, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.85467e-01 
 2.36436e+00 
-9.86320e-01 
 3.90920e-01 
 0.00000e+00 
 0.00000e+00 
-6.47200e-03 
-6.00000e-01 
 2.10000e+00 
-1.00000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.85467e-01 
 2.36436e+00 
-9.86320e-01 
 3.90920e-01 
 0.00000e+00 
 0.00000e+00 
-6.47200e-03 
-6.00000e-01 
 2.10000e+00 
-1.00000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.006087518242927
SET 2
 2, EXTF extended function flag
 4.481286, Tm, Brho=By*Rdipole optimum
 0.979223, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 60.000000, Amps
 0.005666, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.68406e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.68406e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.006209797964157
SET 3
 2, EXTF extended function flag
 5.561356, Tm, Brho=By*Rdipole optimum
 1.215233, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 75.000000, Amps
 0.006403, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.69179e-01 
 2.32110e+00 
-7.20688e-01 
 2.91625e-01 
 0.00000e+00 
 0.00000e+00 
-6.58750e-03 
-6.00000e-01 
 2.10000e+00 
 1.17750e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.69179e-01 
 2.32110e+00 
-7.20688e-01 
 2.91625e-01 
 0.00000e+00 
 0.00000e+00 
-6.58750e-03 
-6.00000e-01 
 2.10000e+00 
 1.17750e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.00728091603647
SET 4
 2, EXTF extended function flag
 7.815184, Tm, Brho=By*Rdipole optimum
 1.707726, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 120.000000, Amps
 0.006720, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.10295e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.10295e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.014932913346763
SET 5
 2, EXTF extended function flag
 8.305671, Tm, Brho=By*Rdipole optimum
 1.814904, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 135.000000, Amps
 0.006733, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.08313e-01 
 2.16372e+00 
-3.60507e-01 
 1.79545e-01 
 0.00000e+00 
 0.00000e+00 
-2.39995e-02 
-6.00000e-01 
 2.10000e+00 
 3.36750e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.08313e-01 
 2.16372e+00 
-3.60507e-01 
 1.79545e-01 
 0.00000e+00 
 0.00000e+00 
-2.39995e-02 
-6.00000e-01 
 2.10000e+00 
 3.36750e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.018189552849340
SET 6
 2, EXTF extended function flag
 9.170518, Tm, Brho=By*Rdipole optimum
 2.003885, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 170.000000, Amps
 0.005665, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.62127e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.62127e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.026785972097535
SET 7
 2, EXTF extended function flag
 9.356754, Tm, Brho=By*Rdipole optimum
 2.044580, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 180.000000, Amps
 0.004861, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.37898e-01 
 1.97670e+00 
-1.69280e-01 
 1.43680e-01 
 0.00000e+00 
 0.00000e+00 
-5.13280e-02 
-6.00000e-01 
 2.10000e+00 
 5.01000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.37898e-01 
 1.97670e+00 
-1.69280e-01 
 1.43680e-01 
 0.00000e+00 
 0.00000e+00 
-5.13280e-02 
-6.00000e-01 
 2.10000e+00 
 5.01000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.029448607550539
SET 8
 2, EXTF extended function flag
 9.534255, Tm, Brho=By*Rdipole optimum
 2.083367, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 190.000000, Amps
 0.006329, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.04378e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.04378e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.032019944823951
END