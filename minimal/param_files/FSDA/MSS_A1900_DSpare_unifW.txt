# A1900 "DSpare" refit of all dipole data in 60 degree bend mode. Probe positions 
# not assigned yet but would likely have no impact since W is uniform in the 
# model. Source of analysis and results:
# /projects/a2400/testMOTER/A1900/A1900_info_from_DeKamp/A19dipole/Refit_FSDA-30deg_2015-03
#version tag:
MSS_V01
 4.576369, Design bend radius
 30.000000, Bend angle
 0.045000, Half-gap
 -6.500000, Entrance edge angle
 0, Entrance curvature
 -6.500000, Exit edge angle
 0, Exit curvature
 2, Number of probe positions
SET 1
 2, EXTF extended function flag
 3.374199, Tm, Brho=By*Rdipole optimum
 0.737309, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 45.000000, Amps
 0.004031, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.51467e-01 
 2.36038e+00 
-9.45867e-01 
 3.75205e-01 
 0.00000e+00 
 0.00000e+00 
-6.03550e-03 
-6.00000e-01 
 2.10000e+00 
 8.25000e-04 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.51467e-01 
 2.36038e+00 
-9.45867e-01 
 3.75205e-01 
 0.00000e+00 
 0.00000e+00 
-6.03550e-03 
-6.00000e-01 
 2.10000e+00 
 8.25000e-04 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.008026293122257
SET 2
 2, EXTF extended function flag
 6.991975, Tm, Brho=By*Rdipole optimum
 1.527843, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 100.000000, Amps
 0.008335, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.88580e-01 
 2.26769e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.88580e-01 
 2.26769e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.013733493886313
SET 3
 2, EXTF extended function flag
 8.429188, Tm, Brho=By*Rdipole optimum
 1.841894, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 140.000000, Amps
 0.010242, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 6.08198e-01 
 2.14041e+00 
-3.35920e-01 
 1.73520e-01 
 0.00000e+00 
 0.00000e+00 
-2.64320e-02 
-6.00000e-01 
 2.10000e+00 
 3.55000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 6.08198e-01 
 2.14041e+00 
-3.35920e-01 
 1.73520e-01 
 0.00000e+00 
 0.00000e+00 
-2.64320e-02 
-6.00000e-01 
 2.10000e+00 
 3.55000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.022594985512050
SET 4
 2, EXTF extended function flag
 9.513159, Tm, Brho=By*Rdipole optimum
 2.078757, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 190.000000, Amps
 0.007394, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 5.78074e-01 
 1.92499e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 5.78074e-01 
 1.92499e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.038265968466748
END