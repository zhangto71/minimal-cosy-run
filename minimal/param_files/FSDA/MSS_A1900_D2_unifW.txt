# A1900 "D2" refit of all dipole data in 60 degree bend mode. Probe positions 
# not assigned yet but would likely have no impact since W is uniform in the 
# model. Source of analysis and results:
# /projects/a2400/testMOTER/A1900/A1900_info_from_DeKamp/A19dipole/Refit_FSDA-30deg_2015-03
#version tag:
MSS_V01
 4.576369, Design bend radius
 30.000000, Bend angle
 0.045000, Half-gap
 -6.500000, Entrance edge angle
 0, Entrance curvature
 -6.500000, Exit edge angle
 0, Exit curvature
 2, Number of probe positions
SET 1
 2, EXTF extended function flag
 2.203164, Tm, Brho=By*Rdipole optimum
 0.481422, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 30.000000, Amps
 0.003891, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.81740e-01 
 2.37016e+00 
-1.06973e+00 
 4.23880e-01 
 0.00000e+00 
 0.00000e+00 
-7.79800e-03 
-6.00000e-01 
 2.10000e+00 
-4.65000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.81740e-01 
 2.37016e+00 
-1.06973e+00 
 4.23880e-01 
 0.00000e+00 
 0.00000e+00 
-7.79800e-03 
-6.00000e-01 
 2.10000e+00 
-4.65000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.006004158448960
SET 2
 2, EXTF extended function flag
 4.373407, Tm, Brho=By*Rdipole optimum
 0.955650, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 60.000000, Amps
 0.007420, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.31376e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.31376e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.005147705462090
SET 3
 2, EXTF extended function flag
 5.747128, Tm, Brho=By*Rdipole optimum
 1.255827, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 80.000000, Amps
 0.008116, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.51655e-01 
 2.31200e+00 
-6.86080e-01 
 2.79480e-01 
 0.00000e+00 
 0.00000e+00 
-7.20800e-03 
-6.00000e-01 
 2.10000e+00 
 1.36000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.51655e-01 
 2.31200e+00 
-6.86080e-01 
 2.79480e-01 
 0.00000e+00 
 0.00000e+00 
-7.20800e-03 
-6.00000e-01 
 2.10000e+00 
 1.36000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.007238832034068
SET 4
 2, EXTF extended function flag
 6.880631, Tm, Brho=By*Rdipole optimum
 1.503513, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 100.000000, Amps
 0.008030, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.42218e-01 
 2.26830e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.42218e-01 
 2.26830e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.009415548344216
SET 5
 2, EXTF extended function flag
 7.698735, Tm, Brho=By*Rdipole optimum
 1.682280, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 120.000000, Amps
 0.006819, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.65251e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.65251e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.013560781043073
SET 6
 2, EXTF extended function flag
 8.349715, Tm, Brho=By*Rdipole optimum
 1.824528, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 140.000000, Amps
 0.006192, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.59123e-01 
 2.14586e+00 
-3.35920e-01 
 1.73520e-01 
 0.00000e+00 
 0.00000e+00 
-2.64320e-02 
-6.00000e-01 
 2.10000e+00 
 3.55000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.59123e-01 
 2.14586e+00 
-3.35920e-01 
 1.73520e-01 
 0.00000e+00 
 0.00000e+00 
-2.64320e-02 
-6.00000e-01 
 2.10000e+00 
 3.55000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.017874502704671
SET 7
 2, EXTF extended function flag
 8.878795, Tm, Brho=By*Rdipole optimum
 1.940140, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 160.000000, Amps
 0.006435, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.48482e-01 
 2.06712e+00 
-2.45920e-01 
 1.54520e-01 
 0.00000e+00 
 0.00000e+00 
-3.76720e-02 
-6.00000e-01 
 2.10000e+00 
 4.28000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.48482e-01 
 2.06712e+00 
-2.45920e-01 
 1.54520e-01 
 0.00000e+00 
 0.00000e+00 
-3.76720e-02 
-6.00000e-01 
 2.10000e+00 
 4.28000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.023113399596688
SET 8
 2, EXTF extended function flag
 9.105442, Tm, Brho=By*Rdipole optimum
 1.989665, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 170.000000, Amps
 0.007525, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 4.28083e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 4.28083e-01 
 2.02337e+00 
-2.05930e-01 
 1.48080e-01 
 0.00000e+00 
 0.00000e+00 
-4.41980e-02 
-6.00000e-01 
 2.10000e+00 
 4.64500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.025629463274580
END