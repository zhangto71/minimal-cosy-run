# A1900 "D3" refit of all dipole data in 60 degree bend mode. Probe positions 
# not assigned yet but would likely have no impact since W is uniform in the 
# model. Source of analysis and results:
# /projects/a2400/testMOTER/A1900/A1900_info_from_DeKamp/A19dipole/Refit_FSDA-30deg_2015-03
#version tag:
MSS_V01
 4.576369, Design bend radius
 30.000000, Bend angle
 0.045000, Half-gap
 -6.500000, Entrance edge angle
 0, Entrance curvature
 -6.500000, Exit edge angle
 0, Exit curvature
 2, Number of probe positions
SET 1
 2, EXTF extended function flag
 4.466850, Tm, Brho=By*Rdipole optimum
 0.976069, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 60.000000, Amps
 0.005236, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.30481e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.30481e-01 
 2.34402e+00 
-8.29520e-01 
 3.31120e-01 
 0.00000e+00 
 0.00000e+00 
-5.63200e-03 
-6.00000e-01 
 2.10000e+00 
 6.30000e-03 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.013593787200382
SET 2
 2, EXTF extended function flag
 6.483437, Tm, Brho=By*Rdipole optimum
 1.416721, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 90.000000, Amps
 0.006482, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.49289e-01 
 2.29161e+00 
-6.19370e-01 
 2.56720e-01 
 0.00000e+00 
 0.00000e+00 
-8.90200e-03 
-6.00000e-01 
 2.10000e+00 
 1.72500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.49289e-01 
 2.29161e+00 
-6.19370e-01 
 2.56720e-01 
 0.00000e+00 
 0.00000e+00 
-8.90200e-03 
-6.00000e-01 
 2.10000e+00 
 1.72500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.016953695679282
SET 3
 2, EXTF extended function flag
 7.002886, Tm, Brho=By*Rdipole optimum
 1.530228, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 100.000000, Amps
 0.006948, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.79205e-01 
 2.26830e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.79205e-01 
 2.26830e+00 
-5.56000e-01 
 2.36000e-01 
 0.00000e+00 
 0.00000e+00 
-1.12000e-02 
-6.00000e-01 
 2.10000e+00 
 2.09000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.019279872571326
SET 4
 2, EXTF extended function flag
 7.813741, Tm, Brho=By*Rdipole optimum
 1.707411, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 120.000000, Amps
 0.006875, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.84007e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.84007e-01 
 2.21292e+00 
-4.39280e-01 
 2.00680e-01 
 0.00000e+00 
 0.00000e+00 
-1.76080e-02 
-6.00000e-01 
 2.10000e+00 
 2.82000e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.023188772203697
SET 5
 2, EXTF extended function flag
 8.715112, Tm, Brho=By*Rdipole optimum
 1.904373, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 150.000000, Amps
 0.006210, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.82613e-01 
 2.10795e+00 
-2.89250e-01 
 1.63000e-01 
 0.00000e+00 
 0.00000e+00 
-3.17500e-02 
-6.00000e-01 
 2.10000e+00 
 3.91500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.82613e-01 
 2.10795e+00 
-2.89250e-01 
 1.63000e-01 
 0.00000e+00 
 0.00000e+00 
-3.17500e-02 
-6.00000e-01 
 2.10000e+00 
 3.91500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.030944288602456
SET 6
 2, EXTF extended function flag
 9.446161, Tm, Brho=By*Rdipole optimum
 2.064117, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 185.000000, Amps
 0.006957, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.10730e-01 
 1.95227e+00 
-1.52207e-01 
 1.42245e-01 
 0.00000e+00 
 0.00000e+00 
-5.51195e-02 
-6.00000e-01 
 2.10000e+00 
 5.19250e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.10730e-01 
 1.95227e+00 
-1.52207e-01 
 1.42245e-01 
 0.00000e+00 
 0.00000e+00 
-5.51195e-02 
-6.00000e-01 
 2.10000e+00 
 5.19250e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.040950624297458
SET 7
 2, EXTF extended function flag
 9.537210, Tm, Brho=By*Rdipole optimum
 2.084013, By at mid-bend position (NomField)
#Probe position(s) and field(s) (Only placeholder for now)
0, 0, 0 
0, 0, 0 
 190.000000, Amps
 0.009175, StdDev for residual of data to ROOT_MSS model
IEE1_ENGEC
 7.00740e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
IEE2_ENGEC
 7.00740e-01 
 1.92711e+00 
-1.35970e-01 
 1.41320e-01 
 0.00000e+00 
 0.00000e+00 
-5.90620e-02 
-6.00000e-01 
 2.10000e+00 
 5.37500e-02 
-9.00000e-01 
 3.30000e-01 
W_COEFS: i,j,W(i,j)
1, 1,  1.042658215683854
END