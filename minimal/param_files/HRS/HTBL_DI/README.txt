
2020-11, MP: 
Calling this dipole design,
D160x10_DH = 160 cm eff. field straight length, 10 cm gap, Horizontal bendMSS_DB1_W11.txt - File developed by S.Noji to emulate field distribution of
Developed for HRS HTBL section. S.Noji fitted COSY MSS element parameters to 
emulate the field.
MSS_DB1_W11.txt - file to use for 22.5 deg bend to HRS branch
MSS_DB1_-20.55deg_W11.txt - file to use for -20.547 deg bend to North branch