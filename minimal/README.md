This directory keeps a minimal list of required files to run a COSY simulation.

- `run.fox`: the COSY lattice input file.
- `1_updated.AMPS`: the device settings files in engineering units, is an output from another program.
- `param_files`: magnet field mapping data.
- `COSY_WRAY_files_to_TTree_v4.c`: ROOT script to process the WRAY output.

