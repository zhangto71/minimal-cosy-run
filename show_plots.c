{
/* 
Generate Canvas with plots and refresh after some pause
*/

  TStyle *myplain = new TStyle("myplain","myplain");
    myplain->SetCanvasColor(kGray);
    myplain->SetCanvasBorderMode(1);
    myplain->SetPadBorderMode(0);
    myplain->SetPadBorderSize(0);
    myplain->SetPadColor(0);
    myplain->SetPadLeftMargin(0.001);
    myplain->SetPadRightMargin(0.001);
    myplain->SetPadTopMargin(0.001);
    myplain->SetPadBottomMargin(0.001);
    myplain->SetTitleColor(0);
    myplain->SetStatColor(0);
  gROOT->SetStyle("myplain");

int cH=900, cV=700;
//cV=cH*1.083;
TCanvas *cPlots1 = new TCanvas("cPlots1", "cPlots1", 30,30, cH,cV);
cPlots1->Divide(1,2);
double xw, yw;
int k=0;
TImage *i1 = TImage::Open("cPlotBmEnv.gif");
TImage *i2 = TImage::Open("cScatPlot.png");
TPad *p1 = new TPad("i", "i", 0,0, 1,1); cPlots1->cd(1); i1->Draw();
TPad *p2 = new TPad("i", "i", 0,0, 1,1); cPlots1->cd(2); i2->Draw();
cPlots1->Modified();
cPlots1->Update();
((TRootCanvas *)cPlots1->GetCanvasImp())->ShowMenuBar();
((TRootCanvas *)cPlots1->GetCanvasImp())->ShowToolBar();
((TRootCanvas *)cPlots1->GetCanvasImp())->ShowStatusBar();

  //Refresh images and update canvas
  long timems, timems0 = gSystem->Now();
  bool cond=1; 
  while(1)
  {
    timems = gSystem->Now();
    cout << (timems-timems0)/1000 << "sec: Pausing ..." <<endl;
    gSystem->Sleep(8000);
    TImage *i1 = TImage::Open("cPlotBmEnv.gif");
    TImage *i2 = TImage::Open("cScatPlot.png");
    cPlots1->cd(1); i1->Draw();
    cPlots1->cd(2); i2->Draw();
    cPlots1->Modified();
    cPlots1->Update();
    cPlots1->SaveAs("show_plots.png"); //takes extra ~2 sec
  }

return;
}

