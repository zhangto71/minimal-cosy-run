#!/bin/bash  
   infile='SLOG.TXT' ; outfile='SLOG.DAT'
   if [ -f $infile ]; then
     echo 'Process '$infile' and generate '$outfile
     sed -e 's/endline/new_line#/' $infile > tmp1
       #get rid of all new line characters
     cat tmp1 | tr -d '\n' > tmp2
     sed 's/new_line#/\n/g' tmp2 > $outfile
     rm -rf tmp1 tmp2 
     #--- Using only ROOT
     #-load module root if not already done
     #if [ "$ROOT_PATH" == "" ]; then module load root ; fi
     #root -l -b -q rootapps/plot_TYPE_SLOG-v1.C
     #root -l -b -q rootapps/plot_TYPE_SLOG-v2.C
     #root -l -b -q plot_TYPE_SLOG-v2.C
   fi
   
