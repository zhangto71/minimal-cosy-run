#!/usr/bin/python3
# chmod +x task_gen.py; ./task_gen.py 
#
# Run a single fit process via TASK in the .fox code

__version__ = '1.1'

import os, shutil, sys, time
from tools import *

#------------------------------------------------------------
if __name__ == '__main__':
  print(" --- start: " + sys.argv[0])

  rc = 'runcosy91.py'   # script to launch cosy
  # Enter names of .fox cosy script and .dat field data to execute
  #cfox1 = "20221128MP_Sn106_prod_refit_wedge_only.fox" ;
  #cfox1 = "20221121_aris_PSv15_test_DB0_124Xe227.fox" ;
  #cfox1 = "20221201_124Xe_k3_nominal.fox"
  #cfox1 = "PSv15_k3_v3.4.9b[124Xe_4.3904Tm].fox"
  #cfox1 = "PSv15[124Xe_4.3904Tm_DB0_DB1-fit1].fox"
  cfox1 = "20221214_PSv15_cycle.fox"
  
  post_process = False 

  #--------------------------------------------------- 0
  if False:
    post_process = False
    task='1.01'  # cycle beam line calc or single pass
    order = 1
    input_list0 =  [ task,   order ]
    print(input_list0)
    flist0 = "fit0.in"
    list_to_file(input_list0, flist0)
    oscom = "chmod +x "+rc+" ; ./"+rc+" "+cfox1+" "+str(post_process)+" < "+flist0
    print(oscom)
    os.system(oscom)
    exit(0)
  #----------------------------------------------- end 0

  #--------------------------------------------------- 1
  # Task 1.10, fit(no fit) 
  if False:
    post_process = True
    task='1.10'
    order = 1
    input_list0 =  [ task,   order ]
    input_list0.append('n')     # enter new ref. particle & wedge
    input_list0.append(0)       # fit method (0=>no fit)
    input_list0.append(180)     # max iterations per cycle
    input_list0.append(1)       # fit cycles
  #----------------------------------------------- end 1

  #--------------------------------------------------- 2
  # Task 1.10, scale only
  if True:
    post_process = False
    task='1.10'
    order = 1
    input_list0 =  [ task,   order ]
    input_list0.append('y')     # enter new ref. particle & wedge
    input_list0.append(3.91040)     # new CHIM
    input_list0.append(123.9)     # new M
    input_list0.append(50)      # new Z
    input_list0.append(50)      # new Q-state
    input_list0.append('y')     # rescale fields
    input_list0.append(0)       # 0=skip refit, 1=refit
    input_list0.append(250)     # fit steps
    input_list0.append(1)       # fit cycles
  #----------------------------------------------- end 2

  print(input_list0)
  flist0 = "fit0.in"
  list_to_file(input_list0, flist0)
  oscom = "chmod +x "+rc+" ; ./"+rc+" "+cfox1+" "+str(post_process)+" < "+flist0
  print(oscom)
  os.system(oscom)
  
  # optionals
  if(post_process):
    os.system(" root -l -b -q rootapps/canvas_of_6_ps_files.c ")
    os.system(" root -l -b -q COSY_WRAY_files_to_TTree_v4.c ")
    os.system(" root -l -b -q rootapps/plot_TYPE_SLOG-v2.C ")
    #os.system(" root -l -b -q plot_TYPE_SLOG-v2x10.C ")
    os.system(" root -l -b -q mpole_data_PROCESS_PS.C ")
    
    # root -l -b -q plot_TYPE_SLOG-v2x10.C COSY_WRAY_files_to_TTree_v4.c
    #os.system(" rm -rf _results; mkdir _results; cp collected_*.csv _results/ ")

else:
  exit()
