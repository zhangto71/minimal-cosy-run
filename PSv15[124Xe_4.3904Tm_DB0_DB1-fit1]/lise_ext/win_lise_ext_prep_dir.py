# Windows python script to replace directory path of map files in .lpp
#  Assumes this script file is in the same directory as .lpp
#  Must have python installed. Double click on this file may be enough.
__version__ = '1.0'

import os

#------------------------------------------------------------
if __name__ == '__main__':
# print(" --- start: " + sys.argv[0])
  subdir0 = "lise_ext/"
  dir0 = os.getcwd()
  dir0 += "\\"
  print(dir0)
  fin = open("lise_skeleton.lpp", 'r')
  fout = open("lise_skeleton2.lpp", 'w')
  for linei in fin:
    linef = linei.replace("lise_ext/", dir0)
    fout.write(linef)
    print(linef)
  fin.close()
  fout.close()
  print('...DONE')  
exit()
