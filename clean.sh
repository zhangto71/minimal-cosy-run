# chmod +x clean.sh; ./clean.sh

#---------------- files after clean.sh script execution
# 20221121_aris_PSv15_test_DB0_124Xe227.fox
# clean.sh
# run_prep.sh
# task_gen.py



rm *~ SLOG_*.* fit*.in MSS_*.DAT *.lis RKLOG.DAT 
rm PS_*.* output_*.csv SLOG.* PA.TXT SB_*.TXT TMP*
rm cosy_*.fox flame.lat dimad_*.* CONTROL OUTPUT.csv LEFF_RADIUS.csv
rm OBJ.TXT tmp foxyinp.dat lattice.*
rm CHIM_*.fox  CHIM_*.dat GLOBAL_MAPS.TXT NEW_*.dat
rm cPlot*.*
rm pic*.ps pic*.pdf pic*.png
rm CB_*.dat NEW*.dat
rm collected_*.csv c*.png

rm task_1.10_PS_REFIT.py tools.py mpole_data_PROCESS.C
rm runcosy*.* task_FIT*.*
#rm process_SLOG.sh
#rm  mpole_data_PROCESS.C
rm task*.sh output.txt lattice.* WRAY.* COSY_WRAY.* canv6*.png

#--- rm links
rm aris-dev aris-optics cosy lnCOSY91.bin param_files rayplot_cosy8anl rootapps 
rm scale_and_refit_process_PS scripts SYSCA.DAT

#--- directories
rm -r lise_* wm_* __py*
