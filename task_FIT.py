#!/usr/bin/python3
# chmod +x task_FIT.py; ./task_FIT.py 
#
# Run a single fit process via TASK in the .fox code

__version__ = '1.1'

import os, shutil, sys, time
from tools import *

#------------------------------------------------------------
if __name__ == '__main__':
  print(" --- start: " + sys.argv[0])

  # Enter names of .fox cosy script and .dat field data to execute
  #  Note that sed replacement is used to set .dat file in .fox file
  order = 1
  cfox1 = "20220906_aris_PSv15k3_CBv05_N4S.fox" ;

  task='1.30' ;

  # FITMODE - only 0,1,3,4 allowed (not 5)   (0 => no FIT)
  #sub_list = [ FITMODE, MAXITERS, FITCYCLES]  FITMODE=0 disables FIT
  sub_list =  [       0,      180,        1 ]

  rc = 'runcosy91.py'   # script to launch cosy
      
  # - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1 - 1
  # Part1: Optimize at a single CHIM=CHIM0 (magnetic rigidity T-m)
  if True:
    # COSY script input
    #input_list = [ TASK, ORDER, NEWREF]
    input_list0 =  [ task,   order,    'n']
    input_list0.append(sub_list[0])
    input_list0.append(sub_list[1])
    input_list0.append(sub_list[2])
    print(input_list0)
    flist0 = "fit0.in"
    list_to_file(input_list0, flist0)
    oscom = "chmod +x "+rc+" ; ./"+rc+" "+cfox1+" < "+flist0
    print(oscom)
    os.system(oscom)

else:
  exit()
